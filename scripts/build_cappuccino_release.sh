#!/bin/bash
set -e

cd `dirname $0`/../app/cappuccino
yarn
sh scripts/post_install.sh
yarn build

