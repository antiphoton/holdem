#!/bin/bash
set -e

cd `dirname $0`/..
rm -rf public
mkdir public
cp -r app/cappuccino/build/* public
rm public/report.html

