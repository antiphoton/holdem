#!/bin/bash
set -e

# https://antiphoton.gitlab.io/cheatsheet/ssh-tunnel/

apt-get update
apt-get install -y openssh-server
mkdir -p /run/sshd

ssh-keygen -A
PASS=$(openssl rand -hex 10)
echo alice:$PASS | chpasswd
echo $PASS
PASS=

echo "AllowTcpForwarding Yes" >>/etc/ssh/sshd_config
/usr/sbin/sshd -p 22

