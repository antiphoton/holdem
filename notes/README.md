## Superhuman AI for multiplayer poker

`brown2019superhuman`

> In this paper we present Pluribus, an AI that we show is stronger than top human professionals in six-player no-limit Texas hold’em poker, the most popular form of poker played by humans.

## Regret Minimization in Games with Incomplete Information

`zinkevich2008regret`

> We show how minimizing counterfactual regret minimizes overall regret, and therefore in self-play can be used to compute a Nash equilibrium.

