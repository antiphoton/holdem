
1. Check dependency digest, which is used in `src/Dockerfile`
```bash
docker pull buildpack-deps:stretch
```

1. Modify [Dockerfile](./src/Dockerfile)

1. Build image
```bash
docker build -t bindgen ./src
```

1. Publish image
```
docker login
docker tag bindgen caoboxiao/bindgen
docker push caoboxiao/bindgen:latest
```

1. Update digest in [.gitlab-ci.yml](../.gitlab-ci.yml).

