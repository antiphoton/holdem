use std::collections::HashMap;
use newton::core::card::Card;
use newton::core::score::Score;

use crate::utils::save_i32_matrix;

fn search(start: u8, remain: u8, cards: &mut Vec<Card>, score_counter: &mut HashMap<Score, u32>) {
    if remain == 0 {
        let score = Score::from_seven_cards(cards);
        if let Some(y) = score_counter.get_mut(&score) {
            *y += 1;
        } else {
            score_counter.insert(score, 1);
        }
    } else {
        for i in start..(52 - remain + 1) {
            cards.push(unsafe {
                Card::from_u8_unchecked(i as u8)
            });
            search(i + 1, remain - 1, cards, score_counter);
            cards.pop();
        }
    }
}

pub fn main() {
    let mut cards: Vec<Card> = vec![];
    let mut score_counter: HashMap::<Score, u32> = HashMap::new();
    search(0, 7, &mut cards, &mut score_counter);
    let mut a: Vec<_> = score_counter.into_iter().collect();
    a.sort();
    a.reverse();
    let mut acc: u32 = 0;
    let mut b: Vec<_> = vec![];
    for (score, cur) in a.into_iter() {
        b.push(vec![
            score.get_rough_score() as i32,
            acc as i32,
            cur as i32,
        ]);
        acc += cur;
    }
    let data = b;
    save_i32_matrix("data", &data);
}

