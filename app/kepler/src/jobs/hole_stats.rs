use std::collections::HashMap;

use newton::core::card::Card;
use newton::core::score::Score;
use crate::loaders::real_score;

use crate::utils::create_all_decks;
use crate::utils::save_string;

fn get_percentile(a: &Vec<(u16, u32)>, i: f64) -> u16 {
    let n = a.len();
    let total = a[n - 1].1 as f64;
    let i = i * total;
    let pos = a.binary_search_by_key(&(i.round() as u32), |x| x.1);
    let pos = match pos {
        Ok(pos) => pos,
        Err(pos) => pos,
    };
    //println!("i = {}, pos = {}, a[pos - 1] = {:?}, a[pos] = {:?}", i, pos, a[pos - 1], a[pos]);
    a[pos].0
}

fn get_percentile_list(a: &Vec<(u16, u32)>, i: Vec<f64>) -> Vec<u16> {
    i.iter().map(|i| {
        get_percentile(a, *i)
    }).collect()
}

#[derive(Debug)]
struct Hole {
    x0: u8,
    x1: u8,
    bits: u32,
}

impl Hole {
    fn from_u8((x0, x1): &(u8, u8)) -> Self {
        let x0 = *x0;
        let x1 = *x1;
        let bits = (1 << x0 as u32) + (1 << x1 as u32);
        Hole {
            x0,
            x1,
            bits,
        }
    }
    fn as_u8(&self) -> (u8, u8) {
        let Hole {
            x0,
            x1,
            ..
        } = *self;
        (x0, x1)
    }
    fn is_interesting(&self) -> bool {
        let Hole {
            x0,
            x1,
            ..
        } = *self;
        x0 % 4 <= 0 && x1 % 4 <= 1
    }
    fn to_string(&self) -> String {
        let a = "23456789TJQKA";
        let a: Vec<_> = a.chars().collect();
        let Hole {
            x0,
            x1,
            ..
        } = *self;
        let r0 = (x0 / 4) as usize;
        let r1 = (x1 / 4) as usize;
        let so = if x0 % 4 == x1 % 4 {
            "s"
        } else if r0 != r1 {
            "o"
        } else {
            " "
        };
        format!("{}{}{}", a[r1], a[r0], so)
    }
}

pub fn main() {
    real_score::init();

    let mut hole_stats: HashMap<(u8, u8), HashMap<u16, u32>> = HashMap::new();

    for hole in create_all_decks(2).into_iter() {
        let hole = (hole[0], hole[1]);
        if Hole::from_u8(&hole).is_interesting() {
            hole_stats.insert(hole, HashMap::<u16, u32>::new());
        }
    }

    let start_time = std::time::Instant::now();

    for (community_id, community) in create_all_decks(5).into_iter().enumerate() {
        if community_id % 10000 == 0 {
            let progress = (community_id as f64 + 0.5) / 2598960.0;
            let cost = start_time.elapsed();
            let need = cost.div_f64(progress).mul_f64(1.0 - progress);
            println!("Progress {:.3}, {} seconds remaining", progress, need.as_secs());
        }
        if community_id % 1 != 0 {
            continue;
        }
        let mut used_by_community = [false; 52];
        for x in community.iter() {
            used_by_community[*x as usize] = true;
        }
        let used_by_community = used_by_community;
        let mut a: Vec<(Hole, u32)> = vec![];
        for hole in create_all_decks(2).into_iter() {
            if used_by_community[hole[0] as usize] || used_by_community[hole[1] as usize] {
                continue;
            }
            let mut cards = community.clone();
            cards.append(&mut hole.clone());
            let cards: Vec<_> = cards.into_iter().map(|x| {
                unsafe {
                    Card::from_u8_unchecked(x)
                }
            }).collect();
            let hole = Hole::from_u8(&(hole[0], hole[1]));
            let score = Score::from_seven_cards(&cards);
            let score = real_score::get(&score);
            a.push((hole, score));
        }
        let a = a;

        for me in a.iter() {
            if me.0.is_interesting() == false {
                continue;
            }
            let mut rank: u16 = 0;
            for enemy in a.iter() {
                if (me.0.bits & enemy.0.bits) > 0 {
                    continue
                }
                if enemy.1 < me.1 {
                    rank += 1
                }
            }
            let counter = hole_stats.get_mut(&me.0.as_u8()).unwrap();
            if let Some(y) = counter.get_mut(&rank) {
                *y += 1;
            } else {
                counter.insert(rank, 1);
            }
        }
    }

    let mut hole_scores: Vec<_> = hole_stats.into_iter().map(|(hole, counter)| {
        let hole = Hole::from_u8(&hole);
        let mut a: Vec<_> = counter.into_iter().collect();
        a.sort();
        for i in 1..a.len() {
            a[i].1 += a[i - 1].1
        }
        let mut p: Vec<f64> = vec![];
        for i in 1..10 {
            p.push(1.0 - (0.5 as f64).powf(1.0 / i as f64));
        }
        println!("{}, {}", hole.to_string(), a[a.len() - 1].1);
        let score = get_percentile_list(&a, p);
        (score[0], score, hole)
    }).collect();

    hole_scores.sort_by_key(|x| x.0);

    let hole_scores = hole_scores;

    for (_, scores, hole) in hole_scores.iter() {
        println!("{}\t{:?}", hole.to_string(), scores);
    }

    let hole_scores: Vec<_> = hole_scores.into_iter().map(|(_, scores, hole)| {
        let scores: Vec<_> = scores.into_iter().map(|x| x.to_string()).collect();
        format!("{},\t{}", hole.to_string(), scores.join(", "))
    }).collect();

    save_string("data.csv", hole_scores.join("\n"));

}

