static mut SEVEN_RANKINGS: Option<Vec<(u32, u32)>> = None;

pub fn init() {
    let s = include_str!("../../run/seven_card_ranking/data.json");
    let data: Vec<Vec<i32>> = serde_json::from_str(s).unwrap();
    let mut data: Vec<_> = data.into_iter().map(|x| {
        (x[0] as u32, (x[1] + x[2] / 2) as u32)
    }).collect();
    data.reverse();
    unsafe {
        SEVEN_RANKINGS = Some(data)
    }
}

pub fn get(score: &newton::core::score::Score) -> u32 {
    let rough_score = score.get_rough_score();
    let ranking = unsafe {
        &SEVEN_RANKINGS
    };
    let ranking = ranking.as_ref().unwrap();
    let i = ranking.binary_search_by_key(&rough_score, |x| x.0).unwrap();
    ranking[i].1
}

