use std::io::prelude::*;

fn search(start: u8, remain: u8, cards: &mut Vec<u8>, result: &mut Vec<Vec<u8>>) {
    if remain == 0 {
        result.push(cards.clone());
    } else {
        for i in start..(52 - remain + 1) {
            cards.push(i);
            search(i + 1, remain - 1, cards, result);
            cards.pop();
        }
    }
}

pub fn create_all_decks(n: usize) -> Vec<Vec<u8>> {
    let mut result: Vec<Vec<u8>> = vec![];
    let mut cards: Vec<u8> = vec![];
    search(0, n as u8, &mut cards, &mut result);
    return result;
}

pub fn save_string(file_name: &str, data: String) {
    std::fs::File::create(file_name).unwrap().write_all(data.as_ref()).unwrap();
}

fn save_i32_matrix_csv(file_name: &str, data: &Vec<Vec<i32>>) {
    let lines: Vec<_> = data.iter().map(|row| {
        let items: Vec<_> = row.iter().map(|x| x.to_string()).collect();
        items.join(", ")
    }).collect();
    let s = lines.join("\n");
    save_string(
        file_name,
        s,
    );
}

pub fn save_i32_matrix(file_name_prefix: &str, data: &Vec<Vec<i32>>) {
    save_string(
        &format!("{}.json", file_name_prefix),
        serde_json::to_string(&data).unwrap(),
    );
    save_i32_matrix_csv(
        &format!("{}.csv", file_name_prefix),
        data,
    );
}

