mod utils;
mod loaders;
mod jobs;

fn main() {
    let args: Vec<_> = std::env::args().collect();
    if let Some(job_name) = args.get(1) {
        match job_name.as_ref() {
            "seven_card_ranking" => jobs::seven_card_ranking::main(),
            "hole_stats"         => jobs::hole_stats::main(),
            _ => (),
        }
    }
}

