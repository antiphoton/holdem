const counts = [
  ['Straight flush' ,     41584],
  ['Four of a kind' ,    224848],
  ['Full house'     ,   3473184],
  ['Flush'          ,   4047644],
  ['Straight'       ,   6180020],
  ['Three of a kind',   6461620],
  ['Two pairs'      ,  31433400],
  ['One pair'       ,  58627800],
  ['High card'      ,  23294460],
];

const benchmarks = (() => {
  const benchmarks = [];
  let acc = 0;
  for (const x of counts) {
    const [name, cur] = x;
    benchmarks.push({
      name,
      low: acc,
      center: acc + cur * 0.5,
      high: acc + cur,
    });
    acc += cur;
  }
  return benchmarks;
})();

const n = benchmarks.length;
const max_high = benchmarks[n - 1].high;

console.log(`
  set terminal svg;
  set output 'out.svg';
  set xtics (${benchmarks.map((item) => {
    return `'' ${max_high - item.high}`;
  }).join(', ')})
  ${benchmarks.map((item, i) => {
    const align = i < 6 ? 'left' : 'center';
    const offset = i < 6 ? 'offset 0, -0.5' : 'offset 0, -1';
    const rotate = i < 6 ? 'rotate by -90' : '';
    return `set label '${item.name}' at ${max_high - item.center}, 0 ${align} ${offset} ${rotate};`;
  }).join('\n')}
  plot 'data.csv' u (${max_high} - $2 - $3 * 0.5):1 w l t '';
`);
