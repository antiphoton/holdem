#!/bin/sh

cd `dirname $0`/../pkg

# Pending issue https://github.com/arabidopsis/typescript-definitions/issues/3
# https://unix.stackexchange.com/questions/20804/in-a-regular-expression-which-characters-need-escaping
sed -i s/Card\ \\+\|\ \\+null/\(Card\ \|\ null\)/g newton.d.ts


WASM_VAR=wasmFilePathStrFromWebpackFileLoader
cat - newton.js > buf <<EOL
import ${WASM_VAR} from './newton_bg.wasm';
EOL

mv buf newton.js

sed -i s/import\\.meta\\.url\\.replace.\\+/${WASM_VAR}\;/g newton.js

