#!/bin/sh

cd `dirname $0`/..

wasm-pack build --target web --dev -- --features "wasm"
sh scripts/.post_build_wasm.sh

