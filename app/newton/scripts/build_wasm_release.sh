#!/bin/sh

cd `dirname $0`/..

wasm-pack build --target web --dev -- --features "wasm"
mv pkg/newton.d.ts pkg/newton.dev.d.ts
wasm-pack build --target web --release -- --features "wasm"
wasm-opt pkg/newton_bg.wasm -Os -o pkg/newton_bg.wasm
wasm-dis pkg/newton_bg.wasm >pkg/newton_bg.wasi
mv pkg/newton.dev.d.ts pkg/newton.d.ts
sh scripts/.post_build_wasm.sh
