use serde::{Serialize, Deserialize};
use crate::core::types::SeatId;
use crate::core::step::Bet;
use crate::core::game::GameImage;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Parameter {
}

pub struct FishAgent {
    parameter: Parameter,
}

impl FishAgent {
    pub fn new(parameter: super::Parameter) -> Self {
        let parameter = serde_json::from_value(parameter.0).unwrap();
        FishAgent {
            parameter,
        }
    }
}

impl super::Agent for FishAgent {
    fn get_parameter_as_yaml(&self) -> serde_yaml::Value {
        serde_yaml::to_value(self.parameter.clone()).unwrap()
    }
    fn on_request_bet(&self, _game_image: &GameImage, _seat_id: SeatId, _seeds: &Vec<u32>) -> Bet {
        Bet::Call
    }
}

