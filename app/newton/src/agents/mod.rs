pub mod fish;
pub mod honest;

use serde::{Serialize, Deserialize};
use serde_json::{Map, Value};

use crate::core::types::SeatId;
use crate::core::step::Bet;
use crate::core::game::GameImage;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Parameter(Value);

impl Parameter {
    pub fn new_empty() -> Self {
        Parameter(Value::Object(Map::new()))
    }
    pub fn from_concat(a: Vec<&Self>) -> Self {
        let mut y: Map<String, Value> = Map::new();
        for x in a.iter().rev() {
            match x.0 {
                Value::Null => (),
                _ => {
                    let m = x.0.as_object().unwrap();
                    for (k, v) in m.iter() {
                        if y.contains_key(k) == false {
                            y.insert(k.clone(), v.clone());
                        }
                    }
                },
            };
        }
        Self(Value::Object(y))
    }
}

pub trait Agent {
    fn get_parameter_as_yaml(&self) -> serde_yaml::Value;
    fn get_parameter_as_yaml_string(&self, indent: i8) -> String {
        let parameter = self.get_parameter_as_yaml();
        let parameter = match parameter {
            serde_yaml::Value::Mapping(mapping) => {
                let mapping = mapping.into_iter().filter(|(_, value)| {
                    match value {
                        serde_yaml::Value::Null => false,
                        _ => true,
                    }
                }).collect();
                serde_yaml::Value::Mapping(mapping)
            },
            _ => {
                unreachable!()
            },
        };
        let s = serde_yaml::to_string(&parameter).unwrap();
        let lines: Vec<_> = s.split('\n').skip(1).map(|x| {
            let mut line = String::new();
            for _ in 0..indent {
                line.push(' ');
            }
            line.push_str(x);
            line
        }).collect();
        lines.join("\n")
    }
    fn on_request_bet(&self, game_image: &GameImage, seat_id: SeatId, seeds: &Vec<u32>) -> Bet;
}

pub fn new(name: &str, parameter: Parameter) -> Box<dyn Agent + std::marker::Send> {
    match name {
        "fish" => Box::new(fish::FishAgent::new(parameter)),
        "honest" => Box::new(honest::HonestAgent::new(parameter)),
        _ => panic!("Unknown agent name"),
    }
}

