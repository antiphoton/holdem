use serde::{Serialize, Deserialize};
use crate::core::types::SeatId;
use crate::core::types::Amount;
use crate::core::deck::Deck;
use crate::core::step::Bet;
use crate::core::score::Score;
use crate::core::game::GameImage;


#[derive(Debug, Clone, Serialize, Deserialize)]
struct Parameter {
    arrogant: Option<f64>,
    use_pot_amount: Option<bool>,
    raise_multiplier: Option<f64>,
}

pub struct HonestAgent {
    parameter: Parameter,
}

impl HonestAgent {
    pub fn new(parameter: super::Parameter) -> Self {
        let parameter = serde_json::from_value(parameter.0).unwrap();
        HonestAgent {
            parameter,
        }
    }
}

const MAX_EVALUATE: usize = 20000;

impl super::Agent for HonestAgent {
    fn get_parameter_as_yaml(&self) -> serde_yaml::Value {
        serde_yaml::to_value(self.parameter.clone()).unwrap()
    }
    fn on_request_bet(&self, game_image: &GameImage, seat_id: SeatId, seeds: &Vec<u32>) -> Bet {
        let n_player = game_image.get_players().iter().filter_map(|player| {
            if player.is_active() {
                Some(())
            } else {
                None
            }
        }).count();
        let my_hole = game_image.get_hole(seat_id);
        let mut cards = game_image.get_community();
        let m = cards.len();
        let mut deck = Deck::from_closed([cards.clone(), my_hole.clone()].concat(), seeds);
        let mut total_win: usize = 0;
        let mut n_sample: usize = 0;
        let mut n_evaluate: usize = 0;
        while n_evaluate < MAX_EVALUATE {
            deck = deck.fork(seeds, None);
            for _ in m..5 {
                cards.push(deck.next_card().unwrap());
            }

            cards.push(my_hole[0]);
            cards.push(my_hole[1]);
            n_evaluate += 1;
            let my_score = Score::from_seven_cards(&cards);
            cards.pop();
            cards.pop();

            let mut win = true;
            let mut n_move: u8 = 0;
            for _ in 1..n_player {
                cards.push(deck.next_card().unwrap());
                cards.push(deck.next_card().unwrap());
                n_move += 2;
                let enemy_score = Score::from_seven_cards(&cards);
                n_evaluate += 1;
                cards.pop();
                cards.pop();
                if enemy_score > my_score {
                    win = false;
                    break;
                }
            }
            for _ in 0..n_move {
                deck.undo_card();
            }


            for _ in m..5 {
                deck.undo_card();
                cards.pop();
            }

            n_sample += 1;
            if win {
                total_win += 1;
            }
        }
        let win_rate = total_win as f64 / n_sample as f64;

        let distance_to_call = game_image.get_distance_to_call(seat_id);

        let arrogant = self.parameter.arrogant.unwrap_or(-0.1);
        let confidence = win_rate * (n_player as f64) * (1.0 + arrogant);

        if distance_to_call == 0 {
            if confidence < 1.0 {
                Bet::Call
            } else {
                let raise_multiplier = self.parameter.arrogant.unwrap_or(2.0);
                let base = if self.parameter.use_pot_amount.unwrap_or(true) {
                    game_image.get_pot() as f64 / n_player as f64
                } else {
                    game_image.get_min_raise() as f64
                };
                let raise = base * (confidence - 1.0) * raise_multiplier;
                Bet::Raise(raise as Amount)
            }
        } else {
            if confidence < 1.0 {
                Bet::Fold
            } else {
                Bet::Call
            }
        }
    }
}

