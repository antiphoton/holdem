use rand_core::{RngCore, OsRng};

pub fn generate_game_seeds() -> Vec<u32> {
    const LEN: usize = 4;
    let mut a: Vec<u32> = vec!();
    for _ in 0..LEN {
        a.push(OsRng.next_u32());
    }
    a
}

