use serde::{Serialize, Deserialize};
use typescript_definitions::TypescriptDefinition;
#[allow(unused_imports)]
use wasm_bindgen::prelude::*;

use super::types::SeatId;
use super::types::Amount;

#[derive(Clone, Copy, Serialize, TypescriptDefinition)]
pub enum Waiting {
    Player(SeatId, Option<Amount>),
    Dealer,
    Ended,
}

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize, TypescriptDefinition)]
pub enum Bet {
    Fold,
    Call,
    Raise(Amount),
    BlindRaise(Amount),
}

#[derive(Clone, Copy, Serialize, Deserialize, TypescriptDefinition)]
pub struct Step {
    pub seat: SeatId,
    pub amount: Amount,
    pub bet: Bet,
}

