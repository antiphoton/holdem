extern crate wasm_bindgen;
use wasm_bindgen::prelude::*;

pub type SeatId = u8;
pub type Amount = u32;

#[wasm_bindgen(typescript_custom_section)]
const _: &'static str = r#"
export type SeatId = number;
export type Amount = number;
"#;

