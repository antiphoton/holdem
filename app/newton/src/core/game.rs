use serde::{Serialize, Deserialize};
use typescript_definitions::TypescriptDefinition;
use wasm_bindgen::prelude::*;

use super::types::SeatId;
use super::types::Amount;
use super::card::Card;
use super::deck::Deck;
use super::step::Waiting;
use super::step::Bet;
use super::step::Step;
use super::score::Score;

#[derive(Copy, Clone, PartialEq, PartialOrd, Serialize, Deserialize, TypescriptDefinition)]
pub enum Round {
    PreFlop  = 0,
    Flop     = 1,
    Turn     = 2,
    River    = 3,
    Showdown = 4,
}

impl Round {
    fn next(&self) -> Round {
        match *self {
            Round::PreFlop => Round::Flop,
            Round::Flop    => Round::Turn,
            Round::Turn    => Round::River,
            Round::River   => Round::Showdown,
            _ => panic!(),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, TypescriptDefinition)]
pub struct Reward {
    pub bet: Amount,
    pub amount: Amount,
    score: Score,
}

macro_rules! define_player_struct {
    ($name:ident < $card_type: ty >) => {
        #[derive(Serialize, Deserialize, TypescriptDefinition)]
        pub struct $name {
            pub hole: [$card_type; 2],
            pocket: Amount,
            mocked: bool,
            tabled: bool,
            acted: bool,
            bet: Amount,
        }
    };
}



define_player_struct! { Player<Card> }
define_player_struct! { PlayerImage<Option<Card>> }


macro_rules! define_game_struct {
    ($name:ident < $card_type: ty, $player_type: ty >) => {
        #[wasm_bindgen]
        #[derive(Serialize, Deserialize, TypescriptDefinition)]
        pub struct $name {
            #[serde(rename="_button")]
            button: SeatId,
            #[serde(rename="_bigBlind")]
            big_blind: Amount,
            players: Vec<$player_type>,
            #[serde(rename="isWaitingForDealer")]
            is_waiting_for_dealer: bool,
            community: [$card_type; 5],
            steps: [Vec<Step>; 4],
            round: Round,
            pot: Amount,
            call: Amount,
            #[serde(rename="previousRoundCall")]
            previous_round_call: Amount,
            min_raise: Amount,
            #[serde(rename="_current")]
            current: SeatId,
        }
    };
}

define_game_struct!{ Game<Card, Player> }
define_game_struct!{ GameImage<Option<Card>, PlayerImage> }

impl Player {
    pub fn get_image(&self, visible: bool) -> PlayerImage {
        let hole = if visible {
            [Some(self.hole[0]), Some(self.hole[1])]
        } else {
            [None, None]
        };
        PlayerImage {
            hole,
            pocket: self.pocket,
            mocked: self.mocked,
            tabled: self.tabled,
            acted: self.acted,
            bet: self.bet,
        }
    }
}

impl Game {
    pub fn get_image(&self, only_seat: Option<SeatId>) -> GameImage {
        let round = self.round;

        let player_images: Vec<_> = self.players.iter().enumerate().map(|(seat_id, player)| {
            let visible = match only_seat {
                Some(only_seat) => only_seat == seat_id as SeatId,
                None => true,
            };
            player.get_image(visible || round == Round::Showdown)
        }).collect();

        let mut community_image: [Option<Card>; 5] = [None; 5];
        if round >= Round::Flop {
            community_image[0] = Some(self.community[0]);
            community_image[1] = Some(self.community[1]);
            community_image[2] = Some(self.community[2]);
        }
        if round >= Round::Turn {
            community_image[3] = Some(self.community[3]);
        }
        if round >= Round::River {
            community_image[4] = Some(self.community[4]);
        }
        GameImage {
            button: self.button,
            big_blind: self.big_blind,
            players: player_images,
            is_waiting_for_dealer: self.is_waiting_for_dealer,
            community: community_image,
            steps: self.steps.clone(),
            round,
            pot: self.pot,
            call: self.call,
            previous_round_call: self.previous_round_call,
            min_raise: self.min_raise,
            current: self.current,
        }
    }
    pub fn new(pockets: Vec<Amount>, button: SeatId, big_blind: Amount, seeds: &Vec<u32>) -> Self {
        let mut deck = Deck::new(seeds);
        let players: Vec<_> = pockets.iter().map(|pocket| {
            Player {
                hole: deck.next_two_cards(),
                pocket: *pocket,
                mocked: false,
                tabled: false,
                acted: false,
                bet: 0,
            }
        }).collect();
        let community = deck.next_five_cards();
        let mut game = Game {
            button,
            big_blind,
            players,
            is_waiting_for_dealer: false,
            community,
            steps: [vec!(), vec!(), vec!(), vec!()],
            round: Round::PreFlop,
            pot: 0,
            call: 0,
            previous_round_call: 0,
            min_raise: big_blind,
            current: button,
        };
        game.proceed_to_next_seat();
        if game.count_unmocked_number() <= 1 {
            game.round = Round::Showdown;
        }
        game.reset_round();
        game
    }
    pub fn get_waiting(&self) -> Waiting {
        if self.round != Round::Showdown {
            if self.is_waiting_for_dealer {
                Waiting::Dealer
            } else {
                let min_raise = if self.is_current_player_allowed_to_raise() {
                    Some(self.min_raise)
                } else {
                    None
                };
                Waiting::Player(self.current, min_raise)
            }
        } else {
            Waiting::Ended
        }
    }
    pub fn resume_by_dealer(&mut self) {
        if self.is_waiting_for_dealer == false {
            return ;
        }
        self.is_waiting_for_dealer = false;
        self.round = self.round.next();
        self.reset_round();
    }
    pub fn resume_by_player(&mut self, bet: Bet) {

        if self.round == Round::Showdown {
            return ;
        }
        if self.is_waiting_for_dealer {
            return ;
        }
        let (bet, amount) = self.get_effective_bet(bet);
        self.players[self.current as usize].acted = true;
        self.push_step(bet, amount);
        self.proceed_to_next_seat();
        if self.count_unmocked_number() <= 1 {
            self.round = Round::Showdown;
        } else {
            if self.is_current_player_done() {
                self.is_waiting_for_dealer = true;
            }
        }
    }
    pub fn get_rewards(&self, include_all_scores: bool) -> Vec<Reward> {
        let a: Vec<_> = self.players.iter().map(|p| PlayerForRewards::from_player(&p)).collect();
        calculate_rewards(&self.community, a, include_all_scores)
    }
    fn cp(&self) -> &Player {
        &self.players[self.current as usize]
    }
    fn cpm(&mut self) -> &mut Player {
        &mut self.players[self.current as usize]
    }
    fn is_current_player_allowed_to_raise(&self) -> bool {
        let distance_to_call = self.call - self.cp().bet;
        self.cp().acted == false || distance_to_call >= self.min_raise
    }
    fn get_effective_bet(&mut self, bet: Bet) -> (Bet, Amount) {
        match bet {
            Bet::Fold => {
                //let mut player = &mut self.players[self.current as usize];
                self.cpm().mocked = true;
                (Bet::Fold, 0)
            },
            Bet::Call => {
                let avaiable = self.cp().pocket;
                let needed = self.call - self.cp().bet;
                let x = std::cmp::min(avaiable, needed);
                self.move_pocket_to_bet(x);
                (Bet::Call, x)
            },
            Bet::Raise(attemp_raise) => {
                let distance_to_call = self.call - self.cp().bet;
                let allowed_to_raise = self.is_current_player_allowed_to_raise();
                let x_call = std::cmp::min(self.cp().pocket, distance_to_call);
                self.move_pocket_to_bet(x_call);
                let able_to_raise = self.cp().pocket > 0;
                if allowed_to_raise && able_to_raise {
                    let x_raise = std::cmp::min(self.cp().pocket, std::cmp::max(self.min_raise, attemp_raise));
                    self.move_pocket_to_bet(x_raise);
                    self.min_raise = std::cmp::max(self.min_raise, x_raise);
                    self.call += x_raise;
                    (Bet::Raise(x_raise), x_call + x_raise)
                } else {
                    (Bet::Call, x_call)
                }
            },
            Bet::BlindRaise(_) => {
                panic!()
            },
        }
    }
    fn reset_round(&mut self) {
        self.current = self.button;
        self.proceed_to_next_seat();
        self.min_raise = self.big_blind;
        for player in self.players.iter_mut() {
            player.acted = false;
        }
        self.previous_round_call = self.call;
        if self.round == Round::PreFlop {
            self.force_blind_bet();
        }
    }
    fn count_unmocked_number(&self) -> u32 {
        let unmocked: u32 = self.players.iter().map(|player| {
            if player.pocket > 0 && player.mocked == false {
                1
            } else {
                0
            }
        }).sum();
        unmocked
    }
    fn is_current_player_done(&self) -> bool {
        self.cp().acted && self.cp().bet == self.call
    }
    fn proceed_to_next_seat(&mut self) {
        let n = self.players.len() as SeatId;
        let mut x = self.current;
        for _ in 0..n {
            x = (x + 1) % n;
            if self.players[x as usize].pocket > 0 && self.players[x as usize].mocked == false {
                self.current = x;
                return ;
            }
        }
        unreachable!();
    }

    fn move_pocket_to_bet(&mut self, amount: Amount) {
        let i = self.current as usize;
        self.players[i].pocket -= amount;
        self.players[i].bet += amount;
        self.pot += amount;
    }
    fn push_step(&mut self, bet: Bet, amount: Amount) {
        self.steps[self.round as usize].push(Step {
            seat: self.current,
            bet,
            amount,
        });
    }
    fn force_blind_bet(&mut self) {
        if self.players.len() == 2 {
            self.proceed_to_next_seat()
        }
        let x1 = self.big_blind / 2;
        let x2 = self.big_blind;
        self.move_pocket_to_bet(std::cmp::min(self.cp().pocket, x1));
        self.push_step(Bet::BlindRaise(x1), x1);
        self.proceed_to_next_seat();
        self.move_pocket_to_bet(std::cmp::min(self.cp().pocket, x2));
        self.push_step(Bet::BlindRaise(x2 - x1), x2);
        self.proceed_to_next_seat();
        self.call += x2;
    }
}

fn card_array_to_vec(input: &[Option<Card>]) -> Vec<Card> {
    let mut output = vec!();
    for card in input.iter() {
        if let Some(card) = card {
            output.push(*card);
        }
    }
    output
}

impl PlayerImage {
    pub fn is_active(&self) -> bool {
        self.mocked == false && self.pocket > 0
    }
}

impl GameImage {
    pub fn get_hole(&self, seat_id: SeatId) -> Vec<Card> {
        card_array_to_vec(&self.players[seat_id as usize].hole)
    }
    pub fn get_community(&self) -> Vec<Card> {
        card_array_to_vec(&self.community)
    }
    pub fn get_players(&self) -> &Vec<PlayerImage> {
        &self.players
    }
    pub fn get_pot(&self) -> Amount {
        self.pot
    }
    pub fn get_min_raise(&self) -> Amount {
        self.min_raise
    }
    pub fn get_distance_to_call(&self, seat_id: SeatId) -> Amount {
        self.call - self.players[seat_id as usize].bet
    }
}

#[derive(Clone)]
struct PlayerForRewards {
    hole: [Card; 2],
    bet: Amount,
    mocked: bool,
}

impl PlayerForRewards {
    fn from_player(player: & Player) -> Self {
        PlayerForRewards {
            hole: player.hole,
            bet: player.bet,
            mocked: player.mocked,
        }
    }
}



fn calculate_rewards(community: &[Card; 5], players: Vec<PlayerForRewards>, include_all_scores: bool) -> Vec<Reward> {
    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize)]
    struct Candidate {
        bet: Amount,
        mocked: bool,
        score: Score,
        seat: SeatId,
    };
    let mut rewards: Vec<_> = players.iter().map(|player| {
        Reward {
            bet: player.bet,
            amount: 0,
            score: Score::Mocked,
        }
    }).collect();
    let mut candidates: Vec<Candidate> = vec!();
    for (seat, player) in players.iter().enumerate() {
        let bet = player.bet;
        if include_all_scores || bet > 0 {
            let score = Score::HighCard(0b11111);
            candidates.push(Candidate {
                seat: seat as SeatId,
                mocked: player.mocked,
                bet,
                score,
            });
            rewards[seat].score = score;
        }
    }
    let n = candidates.len();
    if n == 1 {
        let pot = candidates.iter().map(|c| c.bet).sum();
        rewards[candidates[0].seat as usize].amount = pot;
        return rewards;
    }
    let mut cards: Vec<Card> = community.to_vec();
    cards.push(unsafe {Card::from_u8_unchecked(0)});
    cards.push(unsafe {Card::from_u8_unchecked(0)});
    for candidate in candidates.iter_mut() {
        let player = &players[candidate.seat as usize];
        let score = if include_all_scores == false && player.mocked {
            Score::Mocked
        } else {
            cards[5] = player.hole[0];
            cards[6] = player.hole[1];
            Score::from_seven_cards(&cards)
        };
        candidate.score = score;
        rewards[candidate.seat as usize].score = score;
    }
    candidates.sort();
    candidates.reverse();
    let candidates = candidates;
    let mut best: Vec<SeatId> = vec!();
    let mut acc_pot: Amount = 0;
    for sub_pot_begin in 0..n {
        if sub_pot_begin > 0 && candidates[sub_pot_begin - 1].bet == candidates[sub_pot_begin].bet {
            continue
        }
        let mut sub_pot_end = sub_pot_begin + 1;
        let mut sub_pot_amount = candidates[sub_pot_begin].bet;
        while sub_pot_end < n {
            if candidates[sub_pot_end].bet != candidates[sub_pot_begin].bet {
                sub_pot_amount -= candidates[sub_pot_end].bet;
                break
            }
            sub_pot_end += 1;
        }
        sub_pot_amount = sub_pot_amount * (sub_pot_end as Amount) + acc_pot;
        acc_pot = 0;
        for i in sub_pot_begin..sub_pot_end {
            use std::cmp::Ordering;
            let cmp = if candidates[i].mocked {
                Ordering::Less
            } else if best.len() == 0{
                Ordering::Greater
            } else {
                candidates[i].score.cmp(&candidates[best[0] as usize].score)
            };
            if cmp == Ordering::Greater {
                best.clear();
            }
            if cmp != Ordering::Less {
                best.push(i as SeatId);
            }
        }

        let m = best.len();
        if m > 0 {
            for i in best.iter() {
                rewards[candidates[*i as usize].seat as usize].amount += sub_pot_amount / (m as Amount);
            }
            rewards[candidates[best[0] as usize].seat as usize].amount += sub_pot_amount % (m as Amount);
        } else {
            acc_pot = sub_pot_amount;
        }
    }

    rewards
}

#[cfg(test)]
mod tests {
    use super::*;

    const BIG_BLIND: Amount = 20;


    #[test]
    fn test_blind() {
        let f = |p: Vec<Amount>, button, bets: Vec<Amount>| {
            let game = Game::new(p, button, BIG_BLIND, &vec!(1));
            for (i, bet) in bets.iter().enumerate() {
                assert_eq!(*bet, game.players[i].bet);
            }
        };

        f(vec!(100, 100), 0, vec!(10, 20));
        f(vec!(100, 100), 1, vec!(20, 10));

        f(vec!(100, 100, 100), 1, vec!(20, 0, 10));

        f(vec!(100, 5, 15), 0, vec!(0, 5, 15));
    }

    #[test]
    fn test_effective_bet() {
        let f = |pockets: Vec<Amount>, in_bet: Bet, expected_bet: Bet, expected_amount: Amount| {
            let mut game = Game::new(pockets, 0, BIG_BLIND, &vec!(1));
            game.resume_by_player(in_bet);
            let Step {
                bet: actual_bet,
                amount: actual_amount,
                ..
            } = game.steps[0][2];
            assert_eq!(actual_bet, expected_bet);
            assert_eq!(actual_amount, expected_amount);

        };
        f(vec!(15, 100), Bet::Call, Bet::Call, 5);
        f(vec!(25, 100), Bet::Call, Bet::Call, 10);
        f(vec!(15, 100), Bet::Raise(1), Bet::Call, 5);
        f(vec!(25, 100), Bet::Raise(1), Bet::Raise(5), 15);
        f(vec!(25, 100), Bet::Raise(6), Bet::Raise(5), 15);
        f(vec!(45, 100), Bet::Raise(6), Bet::Raise(20), 30);
        f(vec!(45, 100), Bet::Raise(22), Bet::Raise(22), 32);
        f(vec!(45, 100), Bet::Raise(28), Bet::Raise(25), 35);
    }

    #[test]
    fn test_rewards() {
        let f = |community: [&'static str; 5], players: Vec<([&str; 2], Amount, bool)>, expected_amounts: Vec<Amount>| {
            let community: [Card; 5] = crate::map_fixed_length_array!(community, |s| Card::from_str(s).unwrap(), Card, 5);
            let players: Vec<_> = players.into_iter().map(|x| {
                let (hole, bet, mocked) = x;
                let hole: [Card; 2] = crate::map_fixed_length_array!(hole, |s| Card::from_str(s).unwrap(), Card, 2);
                PlayerForRewards {
                    hole,
                    bet,
                    mocked,
                }
            }).collect();
            let actual_rewards = calculate_rewards(&community, players.clone(), false);
            let actual_amounts: Vec<_> = actual_rewards.into_iter().map(|x| x.amount).collect();
            assert_eq!(actual_amounts, expected_amounts);
            let actual_rewards = calculate_rewards(&community, players.clone(), true);
            let actual_amounts: Vec<_> = actual_rewards.into_iter().map(|x| x.amount).collect();
            assert_eq!(actual_amounts, expected_amounts);
        };
        let g = |bets: [(Amount, bool); 3], expected_amounts: [Amount; 3]| {
            let community = ["As", "Ks", "Qs", "Ah", "Kh"];
            let players = vec![
                (["Js", "Ts"], bets[0].0, bets[0].1),
                (["Ad", "Kd"], bets[1].0, bets[1].1),
                (["Ac", "Kc"], bets[2].0, bets[2].1),
            ];
            f(community, players, expected_amounts.to_vec());
        };
        g([(100, false), (100, false), (100, false)], [300,   0,   0]);
        g([(100, true ), (100, false), (100, false)], [  0, 150, 150]);
        g([(100, true ), ( 50, false), (100, false)], [  0,  75, 175]);
        g([( 50, false), (100, false), (150, false)], [150,  50, 100]);
        g([( 50, true ), (100, false), (150, false)], [  0, 125, 175]);
        g([( 50, true ), (100, false), (150, true )], [  0, 300,   0]);
        g([( 50, false), (100, true ), (150, false)], [150,   0, 150]);
        g([(100, false), ( 50, true ), (150, false)], [250,   0,  50]);

        f(
            ["7d", "Kd", "3d", "Qc", "3s"],
            vec![
                (["9h", "Qh"], 1564, false),
                (["Jh", "4s"],    0, true ),
                (["Td", "5s"], 1000, true ),
                (["5d", "Kh"],  100, false),
                (["7h", "8s"],  200, true ),
                (["5h", "3h"], 1000, false),
            ],
            vec![564, 0, 0, 0, 0, 3300],
        );
    }
}

