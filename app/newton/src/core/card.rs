use serde::{Serialize, Deserialize};
use typescript_definitions::TypescriptDefinition;
#[allow(unused_imports)]
use wasm_bindgen::prelude::*;

#[derive(Clone, Copy)]
pub struct Suit(u8);

#[derive(Clone, Copy)]
pub struct Rank(u8);

#[derive(Debug, Clone, Copy, Serialize, Deserialize, TypescriptDefinition)]
pub struct Card(u8);

impl Suit {
    unsafe fn from_u8_unchecked(x: u8) -> Self {
        Self(x)
    }
    pub fn from_char(x: char) -> Option<Self> {
        match x {
            'C' | 'c' => Some(Self(0)),
            'D' | 'd' => Some(Self(1)),
            'H' | 'h' => Some(Self(2)),
            'S' | 's' => Some(Self(3)),
            _ => None,
        }
    }
    pub fn from_str(s: &str) -> Option<Self> {
        let a: Vec<_> = s.chars().collect();
        if a.len() == 1 {
            Self::from_char(a[0])
        } else {
            None
        }
    }
    /*
    pub fn from_u8(x: u8) -> Option<Self> {
        if x < 4 {
            Some(Self(x))
        } else {
            None
        }
    }
    */
    pub fn as_u8(&self) -> u8 {
        self.0
    }
}

impl Rank {
    unsafe fn from_u8_unchecked(x: u8) -> Self {
        Self(x)
    }
    pub fn from_char(x: char) -> Option<Self> {
        match x {
            'A' | 'a' => Some(Self(12)),
            'K' | 'k' => Some(Self(11)),
            'Q' | 'q' => Some(Self(10)),
            'J' | 'j' => Some(Self(9)),
            'T' | 't' => Some(Self(8)),
            '9'       => Some(Self(7)),
            '8'       => Some(Self(6)),
            '7'       => Some(Self(5)),
            '6'       => Some(Self(4)),
            '5'       => Some(Self(3)),
            '4'       => Some(Self(2)),
            '3'       => Some(Self(1)),
            '2'       => Some(Self(0)),
            _ => None,
        }
    }
    pub fn from_str(s: &str) -> Option<Self> {
        let a: Vec<_> = s.chars().collect();
        if a.len() == 1 {
            Self::from_char(a[0])
        } else {
            None
        }
    }
    /*
    fn from_u8(x: u8) -> Option<Self> {
        if x < 13 {
            Some(Self(x))
        } else {
            None
        }
    }
    */
    pub fn as_u8(&self) -> u8 {
        self.0
    }
}

impl Card {
    pub fn as_u8(&self) -> u8 {
        self.0
    }
    pub unsafe fn from_u8_unchecked(x: u8) -> Self {
        Card(x)
    }
    pub fn from_str(s: &str) -> Option<Self> {
        let a: Vec<_> = s.chars().collect();
        if a.len() == 2 {
            let rank = Rank::from_char(a[0])?.as_u8();
            let suit = Suit::from_char(a[1])?.as_u8();
            Some(Card(rank * 4 + suit))
        } else {
            None
        }
    }
    /*
    pub fn from_u8(x: u8) -> Option<Self> {
        if x < 52 {
            let card = unsafe {
                Card::from_u8_unchecked(x)
            };
            Some(card)
        } else {
            None
        }
    }
    */
    pub fn suit(&self) -> Suit {
        unsafe {
            Suit::from_u8_unchecked(self.0 % 4)
        }
    }
    pub fn rank(&self) -> Rank {
        unsafe {
            Rank::from_u8_unchecked(self.0 / 4)
        }
    }
}

