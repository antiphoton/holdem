use serde::{Serialize};
use typescript_definitions::TypescriptDefinition;
use wasm_bindgen::prelude::*;

use super::card::Card;

type Bits = u32;

#[wasm_bindgen(typescript_custom_section)]
const _: &'static str = r#"
type Bits = number;
"#;

#[derive(Debug, Hash, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, TypescriptDefinition)]
pub enum Score {
    Mocked,
    HighCard(Bits),
    OnePair(Bits, Bits),
    TwoPairs(Bits, Bits),
    ThreeOfAKind(Bits, Bits),
    Straight(Bits),
    Flush(Bits),
    FullHouse(Bits, Bits),
    FourOfAKind(Bits, Bits),
    StraightFlush(Bits),
}

const STRAIGHT_CHECKS: [(Bits, Bits); 10] = [
    (0b1111100000000, 1 << 12),
    (0b0111110000000, 1 << 11),
    (0b0011111000000, 1 << 10),
    (0b0001111100000, 1 <<  9),
    (0b0000111110000, 1 <<  8),
    (0b0000011111000, 1 <<  7),
    (0b0000001111100, 1 <<  6),
    (0b0000000111110, 1 <<  5),
    (0b0000000011111, 1 <<  4),
    (0b1000000001111, 1 <<  3),
];

impl Score {
    pub fn from_seven_cards(cards: &Vec<Card>) -> Self {
        assert_eq!(cards.len(), 7);
        let suit_count = {
            let mut suit_count = [0; 4];
            for card in cards.iter() {
                suit_count[card.suit().as_u8() as usize] += 1;
            }
            suit_count
        };
        let flush_suit = {
            let mut flush_suit: Option<u8> = None;
            for i in 0..4 {
                if suit_count[i] >= 5 {
                    flush_suit = Some(i as u8);
                    break;
                };
            };
            flush_suit
        };
        let rank_bits = {
            let mut rank_bits: Bits = 0;
            for card in cards.iter() {
                let good = match flush_suit {
                    Some(flush_suit) => flush_suit == card.suit().as_u8(),
                    None => true,
                };
                if good {
                    let x = card.rank().as_u8();
                    rank_bits |= 1 << x;
                }
            }
            rank_bits
        };
        let rank_count = {
            let mut rank_count = [0 as u8; 13];
            for card in cards.iter() {
                let x = card.rank().as_u8();
                rank_count[x as usize] += 1;
            }
            rank_count
        };
        let straight_high = {
            let mut straight_high = None;
            for check in STRAIGHT_CHECKS.iter() {
                if (rank_bits & check.0) == check.0 {
                    straight_high = Some(check.1);
                    break;
                }
            };
            straight_high
        };
        match flush_suit {
            Some(flush_suit) => {
                match straight_high {
                    Some(straight_high) => {
                        Score::StraightFlush(straight_high)
                    },
                    None => {
                        let mut a: Vec<_> = cards.iter().filter_map(|card| {
                            if card.suit().as_u8() == flush_suit {
                                Some(1 << card.rank().as_u8())
                            } else {
                                None
                            }
                        }).collect();
                        a.sort();
                        a.reverse();
                        let mut y: Bits = 0;
                        for i in 0..5 {
                            y |= a[i];
                        };
                        Score::Flush(y)
                    },
                }
            },
            None => {
                let a = {
                    let mut a: Vec<(u8, Bits)> = vec!();
                    for i in 0..13 {
                        if rank_count[i] > 0 {
                            a.push((rank_count[i], 1 << i))
                        }
                    }
                    a.sort();
                    a.reverse();
                    a
                };
                if a[0].0 == 4 {
                    let kicker = if a.len() == 3 {
                        std::cmp::max(a[1].1, a[2].1)
                    } else {
                        a[1].1
                    };
                    Score::FourOfAKind(a[0].1, kicker)
                } else if let Some(straight_high) = straight_high {
                    Score::Straight(straight_high)
                } else if a[0].0 == 3 {
                    if a[1].0 >= 2 {
                        Score::FullHouse(a[0].1, a[1].1)
                    } else {
                        Score::ThreeOfAKind(a[0].1, a[1].1 | a[2].1)
                    }
                } else if a[0].0 == 2 {
                    if a[1].0 == 2 {
                        let kicker = std::cmp::max(a[2].1, a[3].1);
                        Score::TwoPairs(a[0].1 | a[1].1, kicker)
                    } else {
                        Score::OnePair(a[0].1, a[1].1 | a[2].1 | a[3].1)
                    }
                } else {
                    Score::HighCard(a[0].1 | a[1].1 | a[2].1 | a[3].1 | a[4].1)
                }
            },
        }
    }
    pub fn get_rough_score(&self) -> u32 {
        let (x0, x1, x2) = match *self {
            Score::StraightFlush(x1)    => (9, x1,  0),
            Score::FourOfAKind(x1, x2)  => (8, x1, x2),
            Score::FullHouse(x1, x2)    => (7, x1, x2),
            Score::Flush(x1)            => (6, x1,  0),
            Score::Straight(x1)         => (5, x1,  0),
            Score::ThreeOfAKind(x1, x2) => (4, x1, x2),
            Score::TwoPairs(x1, x2)     => (3, x1, x2),
            Score::OnePair(x1, x2)      => (2, x1, x2),
            Score::HighCard(x1)         => (1, x1,  0),
            Score::Mocked               => (0,  0,  0),
        };
        (x0 << 26) + (x1 << 13) + x2
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn bits(a: &str) -> Bits {
        let mut y: Bits = 0;
        for x in a.chars() {
            let x = super::super::card::Rank::from_char(x).unwrap();
            y |= 1 << x.as_u8();
        }
        y
    }

    #[test]
    fn test_from_seven_cards() {
        let f = |cards: Vec<&str>, expected_score: Score| {
            let cards: Vec<_> = cards.iter().map(|s| Card::from_str(s).unwrap()).collect();
            let actual_score = Score::from_seven_cards(&cards);
            assert_eq!(actual_score, expected_score);
        };
        f(vec!("As", "Qs", "2h", "2h", "Js", "Ts", "Ks"), Score::StraightFlush(bits("A"))); 
        f(vec!("As", "Qs", "2s", "5s", "Qh", "3s", "4s"), Score::StraightFlush(bits("5"))); 

        f(vec!("As", "Qs", "5h", "Ah", "Ad", "Ac", "5s"), Score::FourOfAKind(bits("A"), bits("Q"))); 
        f(vec!("As", "Qs", "5h", "Ah", "Ad", "Ac", "7s"), Score::FourOfAKind(bits("A"), bits("Q"))); 

        f(vec!("6s", "6h", "6d", "Js", "Jh", "Jd", "5c"), Score::FullHouse(bits("J"), bits("6"))); 
        f(vec!("6s", "6h", "6d", "Js", "Jh", "5d", "5c"), Score::FullHouse(bits("6"), bits("J"))); 
        f(vec!("6s", "6h", "6d", "Qs", "Jh", "5d", "5c"), Score::FullHouse(bits("6"), bits("5"))); 
        
        f(vec!("6s", "5s", "Js", "Qs", "3s", "5d", "2s"), Score::Flush(bits("QJ653"))); 

        f(vec!("6s", "5s", "4s", "3d", "2s", "6d", "6h"), Score::Straight(bits("6"))); 

        f(vec!("6s", "5s", "4s", "3d", "As", "6d", "6h"), Score::ThreeOfAKind(bits("6"), bits("A5"))); 

        f(vec!("6s", "5s", "4s", "3d", "As", "6d", "5h"), Score::TwoPairs(bits("65"), bits("A"))); 
        f(vec!("6s", "5s", "3s", "3d", "As", "6d", "5h"), Score::TwoPairs(bits("65"), bits("A"))); 

        f(vec!("6s", "5s", "3s", "8d", "As", "6d", "9h"), Score::OnePair(bits("6"), bits("A98"))); 
        
        f(vec!("6s", "5s", "3s", "8d", "As", "7d", "Th"), Score::HighCard(bits("AT876"))); 
    }
}

