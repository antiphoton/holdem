use super::card::Card;
use super::prng::Prng;

const TOTAL: usize = 52;

pub struct Deck {
    cards: Vec<Card>,
    next: usize,
}

impl Deck {
    pub fn new(seeds: &Vec<u32>) -> Self {
        let mut cards: Vec<_> = (0..TOTAL).into_iter().map(|x| {
            unsafe {
                Card::from_u8_unchecked(x as u8)
            }
        }).collect();
        let mut prng = Prng::new(seeds);
        prng.shuffle(&mut cards[..]);
        Deck {
            cards,
            next: 0,
        }
    }
    pub fn from_closed(closed: Vec<Card>, seeds: &Vec<u32>) -> Self {
        let mut cards: Vec<Card> = vec!();
        let mut used = [false; TOTAL];
        for x in closed.into_iter() {
            cards.push(x);
            used[x.as_u8() as usize] = true;
        }
        let used = used;
        let next = cards.len();
        for x in 0..TOTAL {
            if used[x] == false {
                cards.push(unsafe {
                    Card::from_u8_unchecked(x as u8)
                });
            }
        }
        let mut prng = Prng::new(seeds);
        prng.shuffle(&mut cards[next..]);
        Deck {
            cards,
            next,
        }
    }
    pub fn fork(&self, seeds: &Vec<u32>, shuffle_number: Option<usize>) -> Self {
        let mut cards = self.cards.clone();
        let next = self.next;
        let mut prng = Prng::new(seeds);
        let end = std::cmp::min(next + shuffle_number.unwrap_or(TOTAL), TOTAL);
        //TODO: this is not correct
        prng.shuffle(&mut cards[next..end]);
        Deck {
            cards,
            next,
        }
    }
    pub fn next_card(&mut self) -> Option<Card> {
        if self.next < TOTAL - 1 {
            let card = self.cards[self.next];
            self.next += 1;
            Some(card)
        } else {
            None
        }
    }
    pub fn next_two_cards(&mut self) -> [Card; 2] {
        [
            self.next_card().unwrap(),
            self.next_card().unwrap(),
        ]
    }
    pub fn next_five_cards(&mut self) -> [Card; 5] {
        [
            self.next_card().unwrap(),
            self.next_card().unwrap(),
            self.next_card().unwrap(),
            self.next_card().unwrap(),
            self.next_card().unwrap(),
        ]
    }
    pub fn undo_card(&mut self) -> Option<Card> {
        if self.next > 0 {
            let card = self.cards[self.next - 1];
            self.next -= 1;
            Some(card)
        } else {
            None
        }
    }
}

