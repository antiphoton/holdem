extern crate rand_core;
extern crate rand_xoshiro;
use rand_core::RngCore;
use rand_core::SeedableRng;
use rand_xoshiro:: Xoshiro512StarStar;

pub struct Prng {
    rngs: Vec<Xoshiro512StarStar>,
    n: usize,
    i: usize,
}

impl Prng {
    pub fn new(seeds: &Vec<u32>) -> Prng {
        let n = seeds.len();
        let rngs = seeds.iter().map(|x| Xoshiro512StarStar::seed_from_u64(*x as u64)).collect();
        Prng {
            rngs,
            n,
            i: 0,
        }
    }
    pub fn next_u64(&mut self) -> u64 {
        if self.n > 0 {
            let result = self.rngs[self.i].next_u64();
            self.i = (self.i + 1) % self.n;
            result
        } else {
            0
        }
    }
    pub fn next_index_in_range(&mut self, begin: usize, end: usize) -> usize {
        let n = end - begin;
        let i = (self.next_u64() % (n as u64)) as usize;
        begin + i
    }
    pub fn next_item_in_vec<'a, T>(&mut self, mut v: Vec<T>) -> Option<T> {
        let n = v.len();
        if n > 0 {
            Some(v.remove(self.next_index_in_range(0, n)))
        } else {
            None
        }
    }
    pub fn next_f64(&mut self, min: f64, max: f64) -> f64 {
        let x = self.next_u64();
        let x = x as f64 / std::f64::MAX as f64;
        min + (max - min) * x
    }
    pub fn next_permutation(&mut self, size: usize) -> Vec<usize> {
        let mut a: Vec<usize> = vec!();
        for i in (0..size).rev() {
            let j = self.next_index_in_range(0, i + 1);
            if j < i {
                a.swap(i, j);
            }
        }
        a
    }
    pub fn shuffle<T>(&mut self, v: &mut [T]) -> () {
        let n = v.len();

        for i in 0..n {
            let j = self.next_index_in_range(i, n);
            if j > i {
                v.swap(i, j);
            }
        }
    }
}

