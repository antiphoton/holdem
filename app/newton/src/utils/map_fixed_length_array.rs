#[macro_export]
macro_rules! map_fixed_length_array {
    ($a: expr, $f: expr, $t: ty, $n: literal) => {
        {
            use std::mem::{MaybeUninit, transmute};
            let mut b: [MaybeUninit<$t>; $n] = unsafe {
                MaybeUninit::uninit().assume_init()
            };

            for i in 0..$n {
                b[i] = MaybeUninit::new($f($a[i]));
            }

            unsafe {
                transmute::<_, [$t; $n]>(b)
            }
        }
    }
}

