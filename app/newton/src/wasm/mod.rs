pub mod utils;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsValue;
use crate::core::types::Amount;
use crate::core::types::SeatId;
use crate::core::game::Game;
use crate::core::step::Waiting;
use crate::core::step::Bet;
use crate::agents;

#[wasm_bindgen(start)]
pub fn main() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
}

type ObjectSnapshot = Vec<u8>;
type JsGame = ObjectSnapshot;
type JsBet = ObjectSnapshot;

fn serialize<T: ?Sized>(object: &T) -> ObjectSnapshot where T: serde::Serialize {
    bincode::serialize(object).unwrap()
}

fn deserialize<'a, T>(bytes: &'a [u8]) -> T where T: serde::Deserialize<'a> {
    bincode::deserialize(bytes).unwrap()
}

#[wasm_bindgen(js_name = createGame)]
pub fn create_game(pockets: Vec<Amount>, button: SeatId, big_blind: Amount, seeds: Vec<u32>) -> JsGame {
    let game = Game::new(pockets, button, big_blind, &seeds);
    serialize(&game)
}

#[wasm_bindgen(js_name = getGameStatus)]
pub fn get_game_status(game: JsGame, only_seat: Option<SeatId>) -> JsValue {
    let game: Game = deserialize(&game);
    let image = game.get_image(only_seat);
    let waiting = game.get_waiting();
    let rewards = match waiting {
        Waiting::Ended => Some(game.get_rewards(true)),
        _ => None,
    };
    JsValue::from_serde(&(image, waiting, rewards)).unwrap()
}

#[wasm_bindgen(js_name = resumeGameWithActionBytes)]
pub fn resume_game_with_action_bytes(game: JsGame, bet: JsBet) -> JsGame {
    let mut game: Game = deserialize(&game);
    let bet: Bet = deserialize(&bet);
    game.resume_by_player(bet);
    serialize(&game)
}

#[wasm_bindgen(js_name = resumeGameWithActionFold)]
pub fn resume_game_with_action_fold(game: JsGame) -> JsGame {
    let mut game: Game = deserialize(&game);
    game.resume_by_player(Bet::Fold);
    serialize(&game)
}

#[wasm_bindgen(js_name = resumeGameWithActionCall)]
pub fn resume_game_with_action_call(game: JsGame) -> JsGame {
    let mut game: Game = deserialize(&game);
    game.resume_by_player(Bet::Call);
    serialize(&game)
}

#[wasm_bindgen(js_name = resumeGameWithActionRaise)]
pub fn resume_game_with_action_raise(game: JsGame, amount: Amount) -> JsGame {
    let mut game: Game = deserialize(&game);
    game.resume_by_player(Bet::Raise(amount));
    serialize(&game)
}

#[wasm_bindgen(js_name = resumeGameByDealer)]
pub fn resume_game_by_dealer(game: JsGame) -> JsGame {
    let mut game: Game = deserialize(&game);
    game.resume_by_dealer();
    serialize(&game)
}

#[wasm_bindgen(js_name = getGameAgentBet)]
pub fn get_game_agent_bet(game: JsGame, seeds: Vec<u32>, name: &str) -> JsBet {
    let game: Game = deserialize(&game);
    match game.get_waiting() {
        Waiting::Player(seat_id, _) => {
            let image = game.get_image(Some(seat_id));
            let agent = agents::new(name, agents::Parameter::new_empty());
            let bet = agent.on_request_bet(&image, seat_id, &seeds);
            serialize(&bet)
        },
        _ => {
            panic!()
        },
    }
}

