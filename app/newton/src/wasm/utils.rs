use serde::Serialize;
use wasm_bindgen::JsValue;
use web_sys::console;

pub fn log<T: ?Sized>(data: &T) where T: Serialize {

    let js_value = JsValue::from_serde(data);
    match js_value {
        Ok(js_value) => console::log_1(&js_value),
        _ => (),
    }
}
