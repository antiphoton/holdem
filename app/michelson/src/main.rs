mod random;
mod cli;
mod experiment;

fn main() {
    let cli::Args {
        config_file_name,
    } = crate::cli::get_args();
    let experiment = experiment::Experiment::new(&config_file_name);
    experiment.run();
}

