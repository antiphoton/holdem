use rand_core::{RngCore, OsRng};

pub fn next_of_population(populations: &Vec<u32>) -> usize {
    let s: u64 = populations.iter().map(|x| *x as u64).sum();
    let mut y: i64 = (OsRng.next_u64() % s) as i64;
    for (i, x) in populations.iter().enumerate() {
        y -= *x as i64;
        if y < 0 {
            return i;
        }
    }
    unreachable!();
}

