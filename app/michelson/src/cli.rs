#[derive(Debug)]
pub struct Args {
    pub config_file_name: String,
}

pub fn get_args() -> Args {
    let a: Vec<_> = std::env::args().collect();
    assert!(a.len() == 2);
    let config_file_name = a[1].clone();
    Args {
        config_file_name,
    }
}

