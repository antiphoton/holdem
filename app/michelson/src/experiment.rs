use std::time::Duration;
use std::collections::BTreeMap;
use std::sync::{Arc, Mutex};
use serde::{Deserialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize)]
struct Id(usize);

static mut NEXT_GLOBAL_ID: usize = 0;

impl Default for Id {
    fn default() -> Self {
        let id = unsafe {
            let x = NEXT_GLOBAL_ID;
            NEXT_GLOBAL_ID += 1;
            x
        };
        Id(id)
    }
}


#[derive(Debug, Default, Deserialize)]
struct Estimate {
    n: f64,
    s: f64,
    s2: f64,
}

impl Estimate {
    fn get_normalized(&self) -> (f64, f64) {
        let Self {
            n,
            s,
            s2,
        } = self;
        let mean = s / n;
        let variance = s2 - mean.powi(2);
        let std_error = (variance / (n * (n - 1.0))).sqrt();
        (mean, std_error)
    }
    fn push(&mut self, x: f64) {
        self.n += 1.0;
        self.s += x;
        self.s2 += x.powi(2);
    }
    fn append(&mut self, other: Self) {
        self.n += other.n;
        self.s += other.s;
        self.s2 += other.s2;
    }
}

impl std::fmt::Display for Estimate {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let (x, x_err) = self.get_normalized();
        write!(f, "{:8.3} ± {:8.3}", x, x_err)
    }
}

#[derive(Debug, Default, Deserialize)]
struct GroupStats {
    pub income: Estimate,
}

impl GroupStats {
    fn append(&mut self, other: Self) {
        self.income.append(other.income);
    }
}


#[derive(Debug, Clone, Deserialize)]
struct Group {
    #[serde(skip_deserializing)]
    id: Id,
    population: u32,
    parameter: newton::agents::Parameter,
}

struct OutputGroup {
    name: String,
    parameter: String,
    stats: GroupStats,
}


#[derive(Debug, Clone, Deserialize)]
struct Layer {
    #[serde(skip_deserializing)]
    id: Id,
    name: String,
    groups: Vec<Group>,
}

#[derive(Debug, Clone, Deserialize)]
struct Agent {
    #[serde(skip_deserializing)]
    id: Id,
    name: String,
    population: u32,
    layers: Vec<Layer>,
}

#[derive(Debug, Deserialize)]
pub struct Experiment {
    parallelization: u16,
    total_tournament: u32,
    game_per_tournament: u32,
    print_every_second: f64,
    agents: Vec<Agent>,
}

const PLAYERS_IN_TOURNAMENT: usize = 6;
const INITIAL_AMOUNT: newton::core::types::Amount = 1000;
const BIG_BLIND: newton::core::types::Amount = 20;

impl Experiment {
    pub fn new(file_name: &str) -> Self {
        let data = std::fs::read_to_string(file_name).expect("Unable to read config file");
        let experiment: Experiment = serde_yaml::from_str(&data).unwrap();
        experiment
    }
    pub fn run(self) -> () {
        let Experiment {
            parallelization,
            total_tournament,
            game_per_tournament,
            print_every_second,
            agents,
        } = self;
        let output_groups = Arc::new(Mutex::new(BTreeMap::<Id, OutputGroup>::new()));
        if let Ok(mut output_groups) = output_groups.lock() {
            for agent in agents.iter() {
                for layer in agent.layers.iter() {
                    for (i, group) in layer.groups.iter().enumerate() {
                        let name = format!("{} v{}", layer.name, i);
                        let agent = newton::agents::new(&agent.name, group.parameter.clone());
                        let parameter = agent.get_parameter_as_yaml_string(8);
                        output_groups.insert(
                            group.id,
                            OutputGroup {
                                name,
                                parameter,
                                stats: Default::default(),
                            }
                        );
                    }
                }
            }
        } else {
            unreachable!();
        }
        let calculated_tournament = Arc::new(Mutex::new(0));
        let mut handles = vec!();
        for _ in 0..parallelization {
            let output_groups = Arc::clone(&output_groups);
            let agents = agents.clone();
            let calculated_tournament = Arc::clone(&calculated_tournament);
            let handle = std::thread::spawn(move || {
                loop {
                    let new_stats_map = run_tournament(&agents, game_per_tournament);
                    if let (Ok(mut output_groups), Ok(mut calculated_tournament)) = (output_groups.lock(), calculated_tournament.lock()) {
                        for (group_id, new_stats) in new_stats_map.into_iter() {
                            if let Some(output_group) = output_groups.get_mut(&group_id) {
                                output_group.stats.append(new_stats);
                            } else {
                                unreachable!();
                            }
                        }
                        *calculated_tournament += 1;
                        if *calculated_tournament >= total_tournament {
                            break;
                        }
                    }
                }
            });
            handles.push(handle);
        }
        if true {
            let calculated_tournament = Arc::clone(&calculated_tournament);
            let handle = std::thread::spawn(move || {
                let mut efficienty: Estimate = Default::default();
                let mut last_calculted = 0;
                loop {
                    std::thread::sleep(Duration::from_secs_f64(print_every_second));
                    if let (Ok(output_groups), Ok(calculated_tournament)) = (output_groups.lock(), calculated_tournament.lock()) {
                        efficienty.push((*calculated_tournament - last_calculted) as f64 / print_every_second / parallelization as f64);
                        last_calculted = *calculated_tournament;
                        println!("Finish {} tounaments,  average {} per cpu per second", calculated_tournament, efficienty);
                        for (_, output_group) in output_groups.iter() {
                            let OutputGroup {
                                name,
                                parameter,
                                stats,
                            } = output_group;
                            println!("Group {}", name);
                            println!("{}", parameter);
                            println!("    {:10}{}", "income", stats.income);
                        }
                        if *calculated_tournament >= total_tournament {
                            break;
                        }
                    }

                }
            });
            handles.push(handle);
        }
        for handle in handles {
            handle.join().unwrap();
        }
    }
}

type Volunteer = (Box<dyn newton::agents::Agent + std::marker::Send>, Vec<Id>);

fn create_volunteer(agents: &Vec<Agent>) -> Volunteer {
    let a: Vec<_> = agents.iter().map(|agent| agent.population).collect();
    let i = crate::random::next_of_population(&a);
    let Agent {
        name,
        layers,
        ..
    } = &agents[i];
    let mut group_ids = vec!();
    let mut params = vec!();
    for layer in layers.iter() {
        let Layer {
            groups,
            ..
        } = layer;
        let a: Vec<_> = groups.iter().map(|group| group.population).collect();
        let i = crate::random::next_of_population(&a);
        group_ids.push(groups[i].id);
        params.push(&groups[i].parameter);
    }
    let param = newton::agents::Parameter::from_concat(params);
    (
        newton::agents::new(name, param),
        group_ids,
    )
}

fn run_tournament(agents: &Vec<Agent>, game_per_tournament: u32) -> BTreeMap<Id, GroupStats> {
    let mut stats_map = BTreeMap::<Id, GroupStats>::new();
    let volunteers: Vec<_> = (0..PLAYERS_IN_TOURNAMENT).map(|_| {
        create_volunteer(&agents)
    }).collect();
    let mut pockets = vec![INITIAL_AMOUNT; PLAYERS_IN_TOURNAMENT];
    let mut button: newton::core::types::SeatId = 0;
    for _ in 0..game_per_tournament {
        let seeds = newton::random::generate_game_seeds();
        let mut game = newton::core::game::Game::new(pockets.clone(), button, BIG_BLIND, &seeds);
        loop {
            use newton::core::step::Waiting;
            match game.get_waiting() {
                Waiting::Dealer => {
                    game.resume_by_dealer();
                },
                Waiting::Player(seat, _) => {
                    let image = game.get_image(Some(seat));
                    let (newton_agent, _) = &volunteers[seat as usize];
                    let bet = newton_agent.on_request_bet(&image, seat, &seeds);
                    game.resume_by_player(bet);
                },
                Waiting::Ended => {
                    break;
                }
            }
        }
        let rewards = game.get_rewards(false);
        for (seat, reward) in rewards.iter().enumerate() {
            let income: i32 = reward.amount as i32 - reward.bet as i32;
            pockets[seat] = (pockets[seat] as i32 + income) as newton::core::types::Amount;
            let (_, group_ids) = &volunteers[seat];
            for group_id in group_ids.iter() {
                if !stats_map.contains_key(group_id) {
                    stats_map.insert(*group_id, Default::default());
                }
                if let Some(stats) = stats_map.get_mut(group_id) {
                    stats.income.push(income as f64 / BIG_BLIND as f64);
                } else {
                    unreachable!();
                }
            }
        }
        button = (button + 1) % (PLAYERS_IN_TOURNAMENT as u8);
        let alive_player_number = pockets.iter().fold(0, |n, x| {
            n + if *x > 0 {
                1
            } else {
                0
            }
        });
        if alive_player_number < 2 {
            break;
        }
    }
    stats_map
}
