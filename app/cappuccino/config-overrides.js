const util = require('util');
const fs = require('fs');
const lodash = require('lodash');
const {
  override,
} = require('customize-cra');
const webpack = require('webpack');
const WasmPackPlugin = require('@wasm-tool/wasm-pack-plugin');
const {
  BundleAnalyzerPlugin,
} = require('webpack-bundle-analyzer');


const useWebWorker = env => config => {
  const ruleList = config.module.rules.find(rule => rule.oneOf).oneOf;
  const babelLoaderIndex = ruleList.findIndex((rule) => {
    const {
      loader,
    } = rule;
    return loader && loader.match(/(^|\/)babel-loader($|\/)/);
  });
  if (babelLoaderIndex === -1) {
    console.error('Unable to use web worker');
    process.exit(1);
  }
  const babelLoader = ruleList[babelLoaderIndex];
  const workerLoader = {
    test: /\.worker\.(js|mjs|jsx|ts|tsx)$/,
    include: lodash.cloneDeep(babelLoader.include),
    use: [
      'worker-loader',
      {
        loader: lodash.cloneDeep(babelLoader.loader),
        options: lodash.cloneDeep(babelLoader.options),
      },
    ],
  };
  ruleList.splice(babelLoaderIndex, 0, workerLoader);
  return config;
};

const fixWasm = env => config => {
  const ruleList = config.module.rules.find(rule => rule.oneOf).oneOf;
  ruleList.unshift({
    test: /\.wasm$/,
    type: 'javascript/auto',
    use: [
      {
        loader: require.resolve('file-loader'),
        options: {
        },
      },
    ],
  });
  config.plugins.push(
    new webpack.ProvidePlugin({
      TextDecoder: ['text-encoding-utf-8', 'TextDecoder'],
      TextEncoder: ['text-encoding-utf-8', 'TextEncoder'],
    }),
  );

  return config;
};

const ANALYZER_PORT = 8881;

const addBundleAnalyzer = env => config => {
  if (env === 'development') {
    config.plugins.push(new BundleAnalyzerPlugin({
      analyzerMode: 'server',
      analyzerPort: ANALYZER_PORT,
      openAnalyzer: false,
    }));
  } else if (env === 'production') {
    config.plugins.push(new BundleAnalyzerPlugin({
      analyzerMode: 'static',
    }));
  }
  return config;
};

const setWatchOptions = config => {
  config.watchOptions = {
    poll: true,
  };
  return config;
};

const proxyAnalyzerServer = config => {
  config.proxy = {
    ...config.proxy,
    '/analyzer': {
      target: `http://localhost:${ANALYZER_PORT}`,
      pathRewrite: {
        '^/analyzer': '',
      },
      ws: true,
    },
  };
  return config;
};



const save = env => config => {
  fs.writeFileSync('currentConfig.js', `const config = ${util.inspect(config, {depth: Infinity})}`);
  return config;
};

module.exports = {
  webpack: function (config, env) {
    return override(
      useWebWorker(env),
      fixWasm(env),
      addBundleAnalyzer(env),
    )(config);
  },
  devServer: function(configFunction) {
    return function(proxy, allowedHost) {
      const config = configFunction(proxy, allowedHost);
      return override(
        setWatchOptions,
        proxyAnalyzerServer,
      )(config);
    };
  },
};

