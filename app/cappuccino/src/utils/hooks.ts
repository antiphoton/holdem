import React from 'react';

export function usePrevious<T>(value: T): [T | undefined, number] {
  const ref1 = React.useRef<T>();
  const ref2 = React.useRef<[T | undefined, number]>([undefined, 0]);

  React.useEffect(() => {
    if (ref1.current !== value) {
      ref2.current = [ref1.current, Date.now()];
    }
    ref1.current = value;
  }, [value]);

  return ref2.current;
};

export function useAnimationFrame(render: () => boolean): void {
  const ref = React.useRef<number>();

  const animate = React.useCallback(() => {
    const shouldEnd = render();
    if (!shouldEnd) {
      ref.current = requestAnimationFrame(animate);
    }
  }, [render]);

  React.useEffect(() => {
    ref.current = requestAnimationFrame(animate);
    return () => {
      const id = ref.current;
      if (id !== undefined) {
        cancelAnimationFrame(id);
      }
    };
  }, [animate]);
                           
      

};

