
export function randomUint32Array(length: number): Uint32Array {
  const a = new Uint32Array(length);
  window.crypto.getRandomValues(a);
  return a;
};

