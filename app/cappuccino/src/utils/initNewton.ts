import init from 'newton';

let isFinished = false;
let isRunning = false;
let resolveList: any[] = [];

export default async function initNewton() {
  return await new Promise((resolve) => {
    if (isFinished) {
      resolve();
    } else if (isRunning) {
      resolveList.push(resolve);
    } else {
      isRunning = true;
      resolveList.push(resolve);
      init().then(() => {
        isFinished = true;
        isRunning = false;
        resolveList.forEach(f => f());
      });
    }
  });
};

