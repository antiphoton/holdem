export type InferActionType<R extends Function> = R extends (_: any, action: infer A) => any ? A : never;
export type InferDispatchType<R extends Function> = (action: InferActionType<R>) => void;

