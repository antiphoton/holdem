import React from 'react';

import 'typeface-roboto';
import StandAlone from './StandAlone/index';

const App = () => {
  return (
    <StandAlone />
  );
};

export default App;

