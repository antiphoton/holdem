import React from 'react';

import {
  connect,
} from 'react-redux';

import {
  sleep,
} from '../../../../utils/time';
import {
  snapshot$,
  image$,
  waiting$,
  rewards$,
  shouldHideCards$,
} from '../../../../redux/game/selectors';
import {
  State,
  IMappedDispatch,
} from '../../../../redux/rootReducer';
import {
  isWasmAgent,
  createResumeAction,
} from '../../../../models/Agent';
import Tokens from '../../../../components/Tokens/index';
import Card from '../../../../components/Card/index';
import Panel from '../../../../components/Panel/index';
import Player from '../../../../components/Player/index';
import myStyles from './index.module.scss';

const GameImage = ({
  agents,
  buttonSeat,
  shouldHideCards,
  snapshot,
  image,
  waiting,
  rewards,
  dispatch,
}: {
} & ReturnType<typeof mapStateToProps> & IMappedDispatch) => {
  const {
    players,
    community,
    previousRoundCall,
    call: totalCall,
    pot: totalPot,
  } = image;
  const currentRoundBets = players.map((player) => {
    const {
      bet,
    } = player;
    return Math.max(bet - previousRoundCall, 0);
  });
  const accRoundsPot = rewards ? 0 : totalPot - currentRoundBets.reduce((s, x) => s + x, 0);
  const [waitingForDealer, waitingPlayerSeat, minRaise] = ((): [boolean, number | null, number | null] => {
    if (waiting === 'Dealer') {
      return [true, null, null];
    } else if (waiting === 'Ended') {
      return [false, null, null];
    } else {
      return [false, waiting.Player[0], waiting.Player[1]];
    }
  })();
  React.useEffect(() => {
    let cancelled = false;
    if (shouldHideCards === false && waitingPlayerSeat !== null) {
      const agent = agents.get(waitingPlayerSeat)!;
      if (isWasmAgent(agent)) {
        (async () => {
          const [bet] = await Promise.all([
            createResumeAction(agent, snapshot),
            sleep(200),
          ]);
          if (cancelled === false) {
            dispatch(bet);
          }
        })();
      }
    }
    return () => {
      cancelled = true;
    };
  }, [agents, shouldHideCards, snapshot, waitingPlayerSeat, dispatch]);
  React.useEffect(() => {
    let cancelled = false;
    if (waitingForDealer) {
      (async () => {
        await sleep(200);
        if (cancelled === false) {
          dispatch({type: 'RESUME_GAME_BY_DEALER'});
        }
      })();
    }
    return () => {
      cancelled = true;
    };
  }, [snapshot, waitingForDealer, dispatch]);
  const playerComponents = players.map((player, seat) => {
    return (
      <Player
        key={seat}
        agent={agents.get(seat)!}
        player={player}
        blindName={seat === buttonSeat ? 'DEALER' : null}
        currentRoundBet={currentRoundBets[seat]}
        minRaise={minRaise}
        isThinking={waitingPlayerSeat === seat}
        reward={rewards && rewards[seat]}
      />
    );
  });
  const currentAgent = waitingPlayerSeat !== null ? agents.get(waitingPlayerSeat)! : null;
  const currentPlayer = waitingPlayerSeat !== null ? players[waitingPlayerSeat] : null;
  return (
    <div className={myStyles.root}>
      <div className={myStyles.shared}>
        <div>
          <Card card={community[0]} />
          <Card card={community[1]} />
          <Card card={community[2]} />
          <Card card={community[3]} />
          <Card card={community[4]} />
        </div>
        <Tokens
          value={accRoundsPot}
        />
      </div>
      {playerComponents}
      <Panel
        agent={currentAgent}
        player={currentPlayer}
        totalCall={totalCall}
        minRaise={minRaise}
        dispatch={dispatch}
      />
    </div>
  );
};

const mapStateToProps = (state: State) => {
  const {
    tournament: {
      agents,
      buttonSeat,
    },
    game,
  } = state;
  return {
    agents,
    buttonSeat,
    shouldHideCards: shouldHideCards$(game),
    snapshot: snapshot$(game)!,
    image: image$(game)!,
    waiting: waiting$(game)!,
    rewards: rewards$(game)!,
  };
};

export default connect(
  mapStateToProps,
)(GameImage);
