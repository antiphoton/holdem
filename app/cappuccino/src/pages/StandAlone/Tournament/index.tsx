import React from 'react';

import {
  connect,
} from 'react-redux';

import {
  snapshot$,
  image$,
  waiting$,
} from '../../../redux/game/selectors';
import {
  State,
} from '../../../redux/rootReducer';
import GameImage from './GameImage/index';
import Control from './Control';

const Tournament = ({
  agents,
  pockets,
  bigBlindAmount,
  buttonSeat,
  snapshot,
  image,
  waiting,
  onRestartTounament,
}: {
  onRestartTounament: () => void,
} & ReturnType<typeof mapStateToProps>) => {
  if (!image || !snapshot || !waiting) {
    return (
      <p>
        Loading
      </p>
    );
  } else {
    return (
      <React.Fragment>
        <GameImage
        />
        <Control
          onRestartTounament={onRestartTounament}
        />
      </React.Fragment>
    );
  }
};

const mapStateToProps = (state: State) => {
  const {
    tournament,
    game,
  } = state;
  const {
    bigBlindAmount,
    buttonSeat,
    agents,
    pockets,
  } = tournament;
  return {
    agents,
    pockets,
    bigBlindAmount,
    buttonSeat,
    snapshot: snapshot$(game),
    image: image$(game),
    waiting: waiting$(game),
  };
};

export default connect(
  mapStateToProps,
)(Tournament);

