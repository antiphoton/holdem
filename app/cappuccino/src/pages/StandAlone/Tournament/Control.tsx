import React from 'react';

import {
  connect,
} from 'react-redux';
import {
  Button,
  ButtonGroup,
  Menu,
  MenuItem,
} from '@material-ui/core';
import {
  MoreVert,
} from '@material-ui/icons';

import {
  sagaProceedToNextGame,
  sagaRestartGame,
} from '../../../redux/tournament/actions';
import {
  image$,
  rewards$,
} from '../../../redux/game/selectors';
import {
  IMappedDispatch,
  State,
} from '../../../redux/rootReducer';

const Control = ({
  image,
  rewards,
  onRestartTounament,
  dispatch,
}: {
  onRestartTounament: () => void,
} & ReturnType<typeof mapStateToProps> & IMappedDispatch) => {
  const {
    players,
  } = image || {};
  const anchorRef = React.useRef<any>(null);
  const [open, setOpen] = React.useState(false);
  const handleClickNextGame = React.useCallback(() => {
    if (players && rewards) {
      const pockets: number[] = [];
      for (let i = 0; i < players.length; i += 1) {
        pockets.push(players[i].pocket + rewards[i].amount);
      }
      dispatch(sagaProceedToNextGame({
        pockets,
      }));
    }
  }, [players, rewards, dispatch]);
  const handleClickRestartGame = React.useCallback(() => {
    dispatch(sagaRestartGame());
  }, [dispatch]);
  const handleClickMenuSwitch = React.useCallback(() => {
    setOpen(x => !x);
  }, []);
  const hasRewards = rewards !== null;
  React.useEffect(() => {
    if (open === true && hasRewards === false) {
      setOpen(false);
    }
  }, [open, hasRewards]);
  if (hasRewards) {
    return (
      <React.Fragment>
        <ButtonGroup
          ref={anchorRef}
          fullWidth={true}
          variant="contained"
          color="primary"
        >
          <Button
            size="large"
            onClick={handleClickNextGame}
          >
            next game
          </Button>
          <Button
            size="small"
            onClick={handleClickMenuSwitch}
          >
            <MoreVert />
          </Button>
        </ButtonGroup>
        <Menu
          anchorEl={anchorRef.current}
          open={open}
          onClose={handleClickMenuSwitch}
        >
          <MenuItem
            onClick={handleClickRestartGame}
          >
            Play same cards again
          </MenuItem>
          <MenuItem
            onClick={onRestartTounament}
          >
            Start new tournament
          </MenuItem>
        </Menu>
      </React.Fragment>
    );
  } else {
    return null;
  }
};

const mapStateToProps = (state: State) => {
  const {
    game,
  } = state;
  return {
    image: image$(game),
    rewards: rewards$(game),
  };
};

export default connect(
  mapStateToProps,
)(Control);

