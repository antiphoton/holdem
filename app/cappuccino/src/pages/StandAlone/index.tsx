import React from 'react';

import {
  connect,
} from 'react-redux';

import initNewton from '../../utils/initNewton';
import {
  initAgents,
  sagaProceedToNextGame,
} from '../../redux/tournament/actions';
import {
  IMappedDispatch,
  State,
} from '../../redux/rootReducer';
import Tournament from './Tournament/index';

const StandAlone = ({
  playerNumber = 6,
  initialPocket = 10000,
  bigBlindAmount = 200,
  hasAgents,
  hasPockets,
  dispatch,
}: {
  playerNumber?: number,
  initialPocket?: number,
  bigBlindAmount?: number,
} & ReturnType<typeof mapStateToProps> & IMappedDispatch) => {
  const [isWasmReady, setIsWasmReady] = React.useState(false);
  React.useEffect(() => {
    let cancelled = false;
    (async () => {
      await initNewton();
      if (cancelled) {
        return ;
      }
      setIsWasmReady(true);
    })();
    return () => {
      cancelled = true;
    };
  }, []);
  React.useEffect(() => {
    if (hasAgents === false) {
      dispatch(initAgents({
        playerNumber,
        mySeat: 0,
      }));
    }
  }, [playerNumber, hasAgents, dispatch]);
  const handleRestartTounament = React.useCallback(() => {
    dispatch(initAgents({
      playerNumber,
      mySeat: 0,
    }));
    const pockets = Array(playerNumber).fill(initialPocket);
    dispatch(sagaProceedToNextGame({
      pockets,
    }));
  }, [playerNumber, initialPocket, dispatch]);
  React.useEffect(() => {
    if (hasPockets === false) {
      handleRestartTounament();
    }
  }, [hasPockets, handleRestartTounament]);
  if (!isWasmReady || !hasAgents) {
    return (
      <p>
        Loading
      </p>
    );
  } else {
    return (
      <React.Fragment>
        <Tournament
          onRestartTounament={handleRestartTounament}
        />
      </React.Fragment>
    );
  }
};

const mapStateToProps = (state: State) => {
  const {
    tournament: {
      agents,
      pockets,
    },
  } = state;
  return {
    hasAgents: agents.size > 0,
    hasPockets: pockets.size > 0,
  };
};

export default connect(
  mapStateToProps,
)(StandAlone);

