import {
  State,
} from './rootReducer';
import {
  serialize,
  deserialize,
} from './serde';

const DELAY = 100;
const KEY = 'refdgtredws';

let timer: ReturnType<typeof setTimeout> | null = null;

export function save(state: State) {
  if (timer) {
    clearTimeout(timer);
    timer = null;
  }
  timer = setTimeout(() => {
    const buffer = serialize(state);
    const s = buffer.toString('base64');
    window.localStorage.setItem(KEY, s);
    timer = null;
  }, DELAY);
};

export function load(): State | undefined {
  const s = window.localStorage.getItem(KEY) || '';
  const buffer = Buffer.from(s, 'base64');
  try {
    const state = deserialize(buffer);
    return state;
  } catch (e) {
    console.error(e);
  }
  return undefined;
}

