import {
  encode,
  decode,
} from 'msgpack-lite';

import {
  serialize as serializeGame,
  deserialize as deserializeGame,
} from './game/serde';
import {
  serialize as serializeTournament,
  deserialize as deserializeTournament,
} from './tournament/serde';
import {
  StateFactory,
  State,
} from './rootReducer';

export function serialize(state: State): Buffer {
  const {
    game,
    tournament,
  } = state;
  const obj = [
    serializeGame(game),
    serializeTournament(tournament),
  ];
  const buffer = encode(obj);
  return buffer;
};

export function deserialize(buffer: Buffer): State {
  const [
    game,
    tournament,
  ] = decode(buffer);
  return StateFactory({
    game: deserializeGame(game),
    tournament: deserializeTournament(tournament),
  });
};

