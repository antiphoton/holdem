import {
  List,
  Record,
} from 'immutable';

import {
  IAgent,
} from '../../models/Agent';

export const StateFactory = Record({
  agents: List<IAgent>(),
  pockets: List<number>(),
  bigBlindAmount: 200,
  buttonSeat: 0,
});

export type State = ReturnType<typeof StateFactory>;

const initialState = StateFactory();

export default initialState;

