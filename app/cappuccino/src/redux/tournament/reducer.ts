import {
  createReducer,
} from 'deox';

import {
  InferActionType,
  InferDispatchType,
} from '../../utils/types';
import initialState from './initialState';
import {
  initAgents,
  setPockets,
  proceedToNextButton,
  sagaProceedToNextGame,
  sagaRestartGame,
} from './actions';

const reducer = createReducer(initialState, handleAction => [
  handleAction(initAgents,(state, { payload }) => {
    return state.set('agents', payload);
  }),
  handleAction(setPockets, (state, { payload }) => {
    return state.set('pockets', payload);
  }),
  handleAction(proceedToNextButton, (state) => {
    const {
      pockets,
      buttonSeat,
    } = state;
    const n = pockets.size;
    let x = buttonSeat;
    while (true) {
      x = (x + 1) % n;
      if (x === buttonSeat) {
        break;
      }
      if (pockets.get(x)! > 0) {
        break;
      }
    }
    return state.set('buttonSeat', x);
  }),
  handleAction(sagaProceedToNextGame, state => state),
  handleAction(sagaRestartGame, state => state),
]);

export type Action = InferActionType<typeof reducer>;
export type Dispatch = InferDispatchType<typeof reducer>;
export default reducer;
