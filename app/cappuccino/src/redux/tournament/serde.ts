import {
  List,
} from 'immutable';
import {
  encode,
  decode,
} from 'msgpack-lite';

import {
  StateFactory,
  State,
} from './initialState';

export function serialize(state: State): Buffer {
  if (!state) {
    return state;
  }
  return encode(state.toJS());
};

export function deserialize(buffer: Buffer): State {
  const obj = decode(buffer);
  if (!obj) {
    return obj;
  }
  const {
    agents,
    pockets,
    bigBlindAmount,
    buttonSeat,
  } = obj;
  return StateFactory({
    agents: List(agents),
    pockets: List(pockets),
    bigBlindAmount,
    buttonSeat,
  });
};

