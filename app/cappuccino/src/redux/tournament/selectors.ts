import {
  State,
} from './initialState';

export const payloadToResetGame$ = (state: State) => {
  const {
    pockets,
    bigBlindAmount,
    buttonSeat,
  } = state;
  return {
    pockets,
    button: buttonSeat,
    bigBlind: bigBlindAmount,
  };
};

