import {
  select,
  put,
  takeLeading,
  call,
  delay,
} from 'redux-saga/effects';

import {
  randomUint32Array,
} from '../../utils/randomArray';

import initNewton from '../../utils/initNewton';
import {
  setSeeds,
  resetGame,
  setShouldHideCards,
} from '../game/actions';
import {
  payloadToResetGame$,
} from './selectors';
import {
  setPockets,
  proceedToNextButton,
  sagaProceedToNextGame,
  sagaRestartGame,
} from './actions';

function* restartGame() {
  yield put(setShouldHideCards(true));
  const state = yield select();
  const payloadToResetGame = payloadToResetGame$(state.tournament);
  yield put(resetGame(payloadToResetGame));
  yield delay(500);
  yield put(setShouldHideCards(false));
};

function* handleProceedToNextGame({ payload }: ReturnType<typeof sagaProceedToNextGame>) {
  yield call(initNewton);
  const {
    pockets,
  } = payload;
  yield put(setPockets(pockets));
  yield put(proceedToNextButton());
  const seeds = randomUint32Array(8);
  yield put(setSeeds(seeds));
  yield call(restartGame);
};

function* handleRestartGame() {
  yield call(initNewton);
  yield call(restartGame);
}

export default function* main() {
  yield takeLeading(sagaProceedToNextGame, handleProceedToNextGame);
  yield takeLeading(sagaRestartGame, handleRestartGame);
};

