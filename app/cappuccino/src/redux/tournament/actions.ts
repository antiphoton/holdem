import {
  List,
} from 'immutable';
import {
  createActionCreator,
} from 'deox';

import {
  IAgent,
} from '../../models/Agent';

export const initAgents = createActionCreator(
  'INIT_AGENTS',
  resolve => ({
    mySeat = 0,
    playerNumber,
  }: {
    mySeat?: number,
    playerNumber: number,
  }) => {
    const agents: IAgent[] = [];
    for (let i = 0; i < playerNumber; i += 1) {
      if (i === mySeat) {
        agents.push({
          type: 'UI',
        });
      } else {
        agents.push({
          type: 'WASM',
          coreName: 'honest',
          parameter: null,
        });
      }
    }
    return resolve(List(agents));
  },
);

export const setPockets = createActionCreator(
  'SET_POCKETS',
  resolve => (payload: number[]) => resolve(List(payload)),
);

export const proceedToNextButton = createActionCreator(
  'PROCEED_TO_NEXT_BUTTON',
  resolve => () => resolve(),
);

export const sagaProceedToNextGame = createActionCreator(
  'PROCEED_TO_NEXT_GAME',
  resolve => (payload: {
    pockets: number[],
  }) => resolve(payload),
);

export const sagaRestartGame = createActionCreator(
  'RESTART_GAME',
  resolve => () => resolve(),
);

