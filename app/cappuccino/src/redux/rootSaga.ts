import {
  call,
} from 'redux-saga/effects';

import tournamentMain from './tournament/saga';

export default function* rootSaga() {
  yield call(tournamentMain);
}

