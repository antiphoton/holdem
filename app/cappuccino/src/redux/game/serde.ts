import {
  encode,
  decode,
} from 'msgpack-lite';

import {
  StateFactory,
  State,
} from './initialState';

export function serialize(state: State): Buffer {
  if (!state) {
    return state;
  }
  return encode(state.toJS());
};

export function deserialize(buffer: Buffer): State {
  const obj = decode(buffer);
  if (!obj) {
    return obj;
  }
  const {
    viewerSeat,
    seeds,
    snapshot,
  } = obj;
  return StateFactory({
    viewerSeat,
    seeds,
    snapshot,
  });
};

