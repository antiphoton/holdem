import {
  createReducer,
} from 'deox';

import {
  createGame,
  resumeGameWithActionFold,
  resumeGameWithActionCall,
  resumeGameWithActionRaise,
  resumeGameWithActionBytes,
  resumeGameByDealer,
} from 'newton';
import {
  InferActionType,
  InferDispatchType,
} from '../../utils/types';
import {
  setSeeds,
  resetGame,
  resumeWithBetObject,
  resumeWithBetBytes,
  resumeByDealer,
  setShouldHideCards,
} from './actions';
import initialState from './initialState';

const reducer = createReducer(initialState, handleAction => [
  handleAction(setSeeds, (state, { payload }) => {
    const seeds = payload;
    return state.set('seeds', seeds);
  }),
  handleAction(resetGame, (state, { payload }) => {
    const {
      seeds,
    } = state;
    const {
      pockets,
      button,
      bigBlind,
    } = payload;
    const snapshot = createGame(new Uint32Array(pockets.toJS()), button, bigBlind, seeds);
    return state.set('snapshot', snapshot);
  }),
  handleAction(resumeWithBetObject, (state, { payload }) => {
    const {
      snapshot,
    } = state;
    if (snapshot) {
      const {
        bet,
      } = payload;
      let newSnapshot;
      if (bet === 'Fold') {
        newSnapshot = resumeGameWithActionFold(snapshot);
      } else if (bet === 'Call') {
        newSnapshot = resumeGameWithActionCall(snapshot);
      } else if ('Raise' in bet) {
        newSnapshot = resumeGameWithActionRaise(snapshot, bet.Raise);
      } else {
        throw Error();
      }
      return state.set('snapshot', newSnapshot);
    } else {
      return state;
    }
  }),
  handleAction(resumeWithBetBytes, (state, { payload }) => {
    const {
      snapshot,
    } = state;
    if (snapshot) {
      const {
        bet,
      } = payload;
      const newSnapshot = resumeGameWithActionBytes(snapshot, bet);
      return state.set('snapshot', newSnapshot);
    } else {
      return state;
    }
  }),
  handleAction(resumeByDealer, (state) => {
    const {
      snapshot,
    } = state;
    if (snapshot) {
      const newSnapshot = resumeGameByDealer(snapshot);
      return state.set('snapshot', newSnapshot);
    } else {
      return state;
    }
  }),
  handleAction(setShouldHideCards, (state, { payload }) => {
    return state.set('shouldHideCards', payload);
  }),
]);

export type Action = InferActionType<typeof reducer>;
export type Dispatch = InferDispatchType<typeof reducer>;
export default reducer;

