import {
  List,
} from 'immutable';
import {
  createActionCreator,
} from 'deox';
import {
  Bet,
} from 'newton';

export const setSeeds = createActionCreator(
  'SET_SEEDS',
  resolve => (payload: Uint32Array) => resolve(payload),
);

export const resetGame = createActionCreator(
  'RESET_GAME',
  resolve => (payload: {
    pockets: List<number>;
    button: number;
    bigBlind: number;
  }) => {
    return resolve(payload);
  },
);

export const resumeWithBetObject = createActionCreator(
  'RESUME_GAME_WITH_BET_OBJECT',
  resolve => (payload: {
    bet: Bet,
  }) => resolve(payload),
);

export const resumeWithBetBytes = createActionCreator(
  'RESUME_GAME_WITH_BET_BYTES',
  resolve => (payload: {
    bet: Uint8Array,
  }) => resolve(payload),
);

export const resumeByDealer = createActionCreator(
  'RESUME_GAME_BY_DEALER',
);

export const setShouldHideCards = createActionCreator(
  'SET_SHOULD_HIDE_CARDS',
  resolve => (payload: boolean) => resolve(payload),
);

