import {
  Record,
} from 'immutable';

import {
  randomUint32Array,
} from '../../utils/randomArray';

export const StateFactory = Record({
  viewerSeat: 0,
  seeds: randomUint32Array(8),
  snapshot: null as (Uint8Array | null),
  shouldHideCards: false,
});

export type State = ReturnType<typeof StateFactory>;

const initialState = StateFactory();

export default initialState;

