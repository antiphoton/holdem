import cloneDeep from 'lodash/cloneDeep';
import {
  createSelector,
} from 'reselect';
import {
  State,
} from './initialState';
import {
  GameImage,
  Waiting,
  Reward,
  getGameStatus,
} from 'newton';

export const viewerSeat$ = (state: State) => state.viewerSeat;
export const snapshot$ = (state: State) => state.snapshot;
export const shouldHideCards$ = (state: State) => state.shouldHideCards;

const gameStatus$ = createSelector(
  viewerSeat$,
  snapshot$,
  (viewerSeat, snapshot) => {
    const ret = snapshot && (getGameStatus(snapshot, viewerSeat) as [GameImage, Waiting, Reward[]]);
    return ret;
  },
);

export const image$ = createSelector(
  gameStatus$,
  shouldHideCards$,
  (gameStatus, shouldHideCards) => {
    if (gameStatus) {
      const realImage = gameStatus[0];
      if (shouldHideCards) {
        const image = cloneDeep(realImage);
        image.players.forEach((player) => {
          player.hole = player.hole.map((_) => null);
        });
        image.community = image.community.map((_) => null);
        return image;
      } else {
        return realImage;
      }
    } else {
      return null;
    }
  },
);

export const waiting$ = createSelector(
  gameStatus$,
  shouldHideCards$,
  (gameStatus, shouldHideCards) => {
    if (gameStatus) {
      if (shouldHideCards) {
        return 'Ended' as 'Ended';
      } else {
        return gameStatus[1];
      }
    } else {
      return null;
    }
  },
);

export const rewards$ = createSelector(
  gameStatus$,
  (gameStatus) => {
    return gameStatus && gameStatus[2];
  },
);

