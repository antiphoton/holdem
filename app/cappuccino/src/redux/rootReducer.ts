import {
  Record,
} from 'immutable';
import {
  combineReducers
} from 'redux-immutable';

import gameInitialState from './game/initialState';
import tournamentInitialState from './tournament/initialState';
import {
  Action as GameAction,
} from './game/reducer';
import {
  Action as TounamentAction,
} from './tournament/reducer';
import game from './game/reducer';
import tournament from './tournament/reducer';

export const StateFactory = Record({
  game: gameInitialState,
  tournament: tournamentInitialState,
});

const rootReducer = combineReducers({
  game,
  tournament,
}, StateFactory as any);

export type State = ReturnType<typeof rootReducer>;

export type Action = GameAction | TounamentAction;

export type Dispatch = (action: Action) => void;

export interface IMappedDispatch {
  dispatch: Dispatch,
}

export default rootReducer;

