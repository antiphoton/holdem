import {
  createStore,
  applyMiddleware,
} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {
  composeWithDevTools,
} from 'redux-devtools-extension/developmentOnly';

import {
  save,
  load,
} from './localCache';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const initialState = load();

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = composeWithDevTools({
});

const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(
    applyMiddleware(sagaMiddleware),
  ),
);

store.subscribe(() => {
  const state = store.getState();
  save(state);
});

sagaMiddleware.run(rootSaga);

export default store;

