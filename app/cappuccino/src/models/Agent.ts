import PromiseWorker from 'promise-worker';

import {
  Bet,
} from 'newton';
import {
  randomUint32Array,
} from '../utils/randomArray';
import {
  resumeWithBetObject,
  resumeWithBetBytes,
} from '../redux/game/actions';
import {
  Action,
} from '../redux/rootReducer';
//@ts-ignore
import AgentWorker from './Agent.worker';

const promiseWorker = new PromiseWorker(new AgentWorker());

const WASM = 'WASM';
const UI = 'UI';

export interface IWasmAgent {
  type: typeof WASM,
  coreName: string;
  parameter: null;
}

export interface IUiAgent {
  type: typeof UI;
}

export type IAgent = IWasmAgent | IUiAgent;

export function isWasmAgent(agent: IAgent | null): agent is IWasmAgent {
  return agent !== null && agent.type === WASM;
}

export function isUiAgent(agent: IAgent | null): agent is IUiAgent {
  return agent !== null && agent.type === UI;
}

export function getNickName(agent: IAgent): string {
  if (isWasmAgent(agent)) {
    return 'Bot';
  } else if (isUiAgent(agent)) {
    return 'You';
  } else {
    return '';
  }
};

export function createResumeAction(agent: IWasmAgent, game: Uint8Array): Promise<Action>;
export function createResumeAction(agent: IUiAgent, bet: Bet): Action;
export function createResumeAction(agent: IAgent, payload: any): any {
  if (isWasmAgent(agent)) {
    return (async () => {
      const game = payload as Uint8Array;
      const seeds = randomUint32Array(8);
      const bet: Uint8Array = await promiseWorker.postMessage({
        funcName: 'agentBet',
        argList: [game, seeds, agent.coreName],
      });
      return resumeWithBetBytes({
        bet,
      });
    })();
  } else if (isUiAgent(agent)) {
    const bet = payload as Bet;
    return resumeWithBetObject({
      bet,
    });
  } else {
    return {} as Action;
  }
};

