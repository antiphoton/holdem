import registerPromiseWorker from 'promise-worker/register';

import {
  createGame,
  getGameAgentBet,
} from 'newton';

import initNewton from '../utils/initNewton';

initNewton();

registerPromiseWorker(async ({
  funcName,
  argList,
}: {
  funcName: string,
  argList: any[],
}) => {
  await initNewton();
  if (funcName === 'agentBet') {
    return getGameAgentBet(...(argList as [Uint8Array, Uint32Array, string]));
  }
  if (funcName === 'createGame') {
    return createGame(new Uint32Array([100, 100, 100]), 0, 10, new Uint32Array([1]));
  }
});

export default {} as typeof Worker & {new (): Worker};
