import React from 'react';

import classnames from 'classnames';

import {
  PlayerImage,
  Reward as NewtonReward,
} from 'newton';
import {
  IAgent,
  getNickName,
} from '../../models/Agent';
import Card from '../Card/index';
import Tokens from '../Tokens/index';
import {
  CircleName,
} from './Circle';
import Circle from './Circle';
import Reward from './Reward';

import myStyles from './index.module.scss';

const Player = ({
  agent,
  player,
  blindName,
  currentRoundBet,
  minRaise,
  isThinking,
  reward,
}: {
  agent: IAgent,
  player: PlayerImage,
  blindName: CircleName | null,
  currentRoundBet: number,
  minRaise: number | null,
  isThinking: boolean,
  reward: NewtonReward | null,
}) => {
  const {
    pocket,
    mocked,
    hole,
    bet: currentGameBet,
  } = player;

  const circleElement = blindName ? (
    <Circle
      name={blindName}
    />
  ) : null;

  const foldStamp = mocked && (
    <div className={myStyles.foldStamp}>
      FOLD
    </div>
  );

  let rightComponents;
  let displayedPocket: number;
  const displayedScore = reward && reward.score;

  if (reward === null) {
    displayedPocket = pocket;
    rightComponents = (
      <div className={myStyles.bet}>
        <Tokens
          value={currentRoundBet}
        />
      </div>
    );
  } else {
    displayedPocket = pocket + reward.amount;
    rightComponents = (
      <Reward
        score={displayedScore}
        amount={reward.amount - currentGameBet}
      />
    );
  }

  return (
    <div
      className={classnames(
        myStyles.root,
        isThinking && myStyles.thinking,
      )}
    >
      <div className={myStyles.cards}>
        <div className={myStyles.hole}>
          <Card card={hole[0]} mocked={mocked} />
          <Card card={hole[1]} mocked={mocked} />
          {foldStamp}
        </div>
      </div>
      <div className={myStyles.profile}>
        {getNickName(agent)}
      </div>
      <div className={myStyles.pocket}>
        <Tokens
          showZero={true}
          value={displayedPocket}
        />
      </div>
      <div className={myStyles.circles}>
        {circleElement}
      </div>
      {rightComponents}
    </div>
  );
};

export default React.memo(Player);

