import React from 'react';

import classnames from 'classnames';

import myStyles from './Circle.module.scss';

const DEALER = 'DEALER';

export type CircleName = typeof DEALER;

const Circle = ({
  name,
}: {
  name: CircleName,
}) => {
  if (name === DEALER) {
    return (
      <div
        className={classnames(
          myStyles.root,
          myStyles.dealer,
        )}
      >
        D
      </div>
    );
  } else {
    return null;
  }
};

export default React.memo(Circle);

