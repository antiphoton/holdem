import React from 'react';

import classnames from 'classnames';

import {
  Score as NewtonScore,
} from 'newton';

import Tokens from '../Tokens/index';
import myStyles from './Reward.module.scss';

function strinigifyScore(score: NewtonScore) {
  const tag = Object.keys(score)[0];
  if (tag === '0') {
    return '';
  } else {
    const a = tag.split(/(?=[A-Z])/);
    const b = a.map((x) => {
      if (['Of', 'A'].includes(x)) {
        return x.toLowerCase();
      } else {
        return x;
      }
    });
    return b.join(' ');
  }
}

const Reward = ({
  score,
  amount,
}: {
  score: NewtonScore | null,
  amount: number,
}) => {
  const scoreElement = score && (
    <div
      className={classnames(
        myStyles.tag,
        amount > 0 ? myStyles.winner : myStyles.loser,
      )}
    >
      {strinigifyScore(score)}
    </div>
  );
  const amountElment = amount !== 0 && (
    <div className={myStyles.amount}>
      <Tokens
        value={amount}
        showPositiveSign={true}
      />
    </div>
  );
  return (
    <React.Fragment>
      {scoreElement}
      {amountElment}
    </React.Fragment>
  );
};

export default React.memo(Reward);

