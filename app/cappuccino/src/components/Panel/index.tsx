import React from 'react';

import classnames from 'classnames';
import {
  Button,
  ButtonGroup,
  Slider,
} from '@material-ui/core';

import {
  PlayerImage as NewtonPlayer,
} from 'newton';
import {
  IAgent,
  isUiAgent,
  createResumeAction,
} from '../../models/Agent';
import {
  Dispatch,
} from '../../redux/rootReducer';
import myStyles from './index.module.scss';

const Panel = ({
  agent,
  player,
  minRaise,
  totalCall,
  dispatch,
}: {
  agent: IAgent | null,
  player: NewtonPlayer | null,
  totalCall: number,
  minRaise: number | null,
  dispatch: Dispatch,
}) => {
  const [raiseAmount, setRaiseAmout] = React.useState(1);
  const distanceToCall = player !== null ? totalCall - player.bet : 0;
  const maxRaise = player !== null ? player.pocket - distanceToCall : 0;

  const normalizedMinRaise = minRaise !== null ? Math.min(minRaise, maxRaise) : 0;
  const allowedToRaise = minRaise !== null && maxRaise > 0;

  const effectiveRaiseAmount = Math.min(Math.max(raiseAmount, normalizedMinRaise), maxRaise);
  const handleClickFold = React.useCallback(() => {
    if (isUiAgent(agent)) {
      dispatch(createResumeAction(agent, 'Fold'));
    }
  }, [agent, dispatch]);
  const handleClickCall = React.useCallback(() => {
    if (isUiAgent(agent)) {
      dispatch(createResumeAction(agent, 'Call'));
    }
  }, [agent, dispatch]);
  const handleClickRaise = React.useCallback(() => {
    if (isUiAgent(agent) && minRaise !== null) {
      dispatch(createResumeAction(
        agent,
        {
          Raise: effectiveRaiseAmount,
        },
      ));
    }
  }, [agent, minRaise, effectiveRaiseAmount, dispatch]);
  const handleChangeSlider = React.useCallback((_, value) => {
    setRaiseAmout(Math.round(Math.exp(value) - 1));
  }, []);

  const showButtons = agent !== null && isUiAgent(agent);

  return (
    <div>
      <div className={classnames(!showButtons && myStyles.hide)}>
        <div className={myStyles.slider}>
          <Slider
            min={Math.log(1 + normalizedMinRaise)}
            max={Math.log(1 + maxRaise)}
            step={0.05}
            value={Math.log(1 + effectiveRaiseAmount)}
            onChange={handleChangeSlider}
          />
        </div>
        <ButtonGroup
          fullWidth={true}
          size="large"
          color="primary"
          variant="contained"
        >
          <Button onClick={handleClickFold}>
            Fold
          </Button>
          <Button onClick={handleClickCall}>
            {distanceToCall > 0 ? (
              `Call ${distanceToCall}`
            ) : (
              'Check'
            )}
          </Button>
          <Button
            disabled={!allowedToRaise}
            onClick={handleClickRaise}
          >
            {distanceToCall > 0 ? (
              `Raise ${effectiveRaiseAmount}`
            ) : (
              `Bet ${effectiveRaiseAmount}`
            )}
          </Button>
        </ButtonGroup>
      </div>
    </div>
  );
};

export default React.memo(Panel);

