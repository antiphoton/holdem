import React from 'react';

import classnames from 'classnames';
import {
  CSSTransition,
  SwitchTransition,
} from 'react-transition-group';

import myStyles from './index.module.scss';
import colorStyles from './colors.module.scss';
import animationStyles from './animation.module.scss';

const SUIT_CHAR_UNICODE = ['\u2663', '\u2666', '\u2665', '\u2660'];
const SUIT_NAME = ['club', 'diamond', 'heart', 'spade'];
const RANK_CHAR = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'];

const Card = ({
  card,
  mocked = false,
}: {
  card: number | null,
  mocked?: boolean,
}) => {
  const rankText = card !== null ? RANK_CHAR[~~(card / 4)] : '?';
  const suitText = card !== null ? SUIT_CHAR_UNICODE[card % 4] : '?';
  const suitColor = card !== null ? colorStyles[SUIT_NAME[card % 4]] : colorStyles['unknown'];
  return (
    <div
      className={classnames(
        myStyles.root,
        mocked && myStyles.mocked,
      )}
    >
      <SwitchTransition>
        <CSSTransition
          key={card !== null ? 'Y' : 'N'}
          appear={true}
          timeout={200}
          classNames={animationStyles}
        >
          <div
            className={classnames(
              myStyles.card,
              suitColor,
            )}
          >
            <div className={myStyles.rank}>
              {rankText}
            </div>
            <div className={myStyles.suit}>
              {suitText}
            </div>
          </div>
        </CSSTransition>
      </SwitchTransition>
    </div>
  );
};

export default Card;
