import React from 'react';

import classnames from 'classnames';

import {
  usePrevious,
  useAnimationFrame,
} from '../../utils/hooks';
import myStyles from './index.module.scss';

const TIMEOUT = 100;

const Tokens = ({
  value,
  showZero = false,
  showPositiveSign = false,
}: {
  value: number,
  showZero?: boolean,
  showPositiveSign?: boolean,
}) => {
  const effectiveValue = value;
  const [previousValue = 0, updateTime] = usePrevious(effectiveValue);
  const [y, setY] = React.useState(0);
  const update = React.useCallback(() => {
    const t = Math.min((Date.now() - updateTime) / TIMEOUT, 1);
    const x = t;
    const y = Math.round(previousValue * (1 - x) + effectiveValue * x);
    setY(y);
    return t >= 1;
  }, [effectiveValue, previousValue, updateTime]);
  useAnimationFrame(update);

  let s: string;
  if (showPositiveSign && y >= 0) {
    s = `+${y}`;
  } else {
    s = `${y}`;
  }

  return (
    <div
      className={classnames(
        showZero === false && y === 0 && myStyles.hidden,
      )}
    >
        {s}
    </div>
  );
};

export default React.memo(Tokens);

