# Holdem poker

![pipline status](https://gitlab.com/antiphoton/holdem/badges/master/pipeline.svg)

## Online demo

Click [this link](https://antiphoton.gitlab.io/holdem) to see the online demo
of the single-page application.
It is automatically deployed by Gitlab CI.

The source code of front-end web is in [this folder](./app/cappuccino).


## Dev commands

```bash
docker run --rm -it -u alice -v $PWD:/repo -p 8022:22 -w /repo --name holdem caoboxiao/bindgen bash
```

```bash
docker exec -it holdem bash
```

```bash
docker exec -it -u root holdem bash /repo/scripts/init_ssh_tunnel.sh
ssh -p 8022 -nNT -L 8080:127.0.0.1:3000 -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking no" alice@127.0.0.1
```

